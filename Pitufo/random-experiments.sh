java -jar -Xmx1000M pitufo.jar -s="paper_data/Bcic/bcicWithoutCofactors.xml" -i="paper_data/Bcic/inputForBcicRandom.xml" -e="100xBcic" -o -d -ecc -fp -mPMin -ni=100 -stop-no-new=10

java -jar -Xmx1000M pitufo.jar -s="paper_data/Smue/smueWithoutCofactors.xml" -i="paper_data/Smue/inputForSmueRandom.xml" -e="100xSmue" -o -d -ecc -fp -mPMin -ni=100 -stop-no-new=10

java -jar -Xmx1000M pitufo.jar -s="paper_data/carso/carso.xml" -i="paper_data/carso/inputForCarsoRandom.xml" -e="100xCarso" -o -d -ecc -fp -mPMin -ni=100 -stop-no-new=10

java -jar -Xmx1000M pitufo.jar -s="paper_data/Buchnera/bucaiRevisedFiltered.xml" -i="paper_data/Buchnera/inputForBuchneraRandom.xml" -e="100xBuch" -o -d -ecc -fp -mPMin -ni=100 -stop-no-new=10

java -jar -Xmx1000M pitufo.jar -s="paper_data/ecoli/smmWithDirections11.5Corrected.xml" -i="paper_data/ecoli/inputForEcoliRandom.xml" -e="100xColi" -o -d -ecc -fp -mPMin -ni=100 -stop-no-new=10

java -jar -Xmx1000M pitufo.jar -s="paper_data/yeast/yeast.xml" -i="paper_data/yeast/inputForYeastRandom.xml" -e="100xYeast" -o -d -ecc -fp -mPMin -ni=100 -stop-no-new=10

java -jar -Xmx1000M pitufo.jar -s="paper_data/human/HUMAN.xml" -i="paper_data/human/inputForHumanRandom.xml" -e="100xHuman" -o -d -ecc -fp -mPMin -ni=100 -stop-no-new=10
