#!/bin/bash

cd /bge/milreu

java -jar -Xmx1000M pitufo.jar -s="paper_data/yeast/yeast.xml" -i="paper_data/yeast/inputForYeast.xml" -e="yeast" -o -d -ecc -fp -mP -mPMin -mPP -mPPMin;
