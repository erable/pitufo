java -jar -Xmx1000M pitufo.jar -s="paper_data/Bcic/bcicWithoutCofactors.xml" -i="paper_data/Bcic/inputForBcic.xml" -o -d -fdrr -fp -mT >pitufo-bcic.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Bcic/bcicWithoutCofactors.xml" -i="paper_data/Bcic/inputForBcic.xml" -o -d -fdrr -fp -mP >pitufina-bcic.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Bcic/bcicWithoutCofactors.xml" -i="paper_data/Bcic/inputForBcic.xml" -o -d -fdrr -fp -mP -cm >pitufina-min-bcic.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Bcic/bcicWithoutCofactors.xml" -i="paper_data/Bcic/inputForBcic.xml" -o -d -fdrr -fp -mM >papaPitufo-bcic.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Bcic/bcicWithoutCofactors.xml" -i="paper_data/Bcic/inputForBcic.xml" -o -d -fdrr -fp -mM -cm >papaPitufo-min-bcic.out

java -jar -Xmx1000M pitufo.jar -s="paper_data/Smue/smueWithoutCofactors.xml" -i="paper_data/Smue/inputForSmue.xml" -o -d -fdrr -fp -mT >pitufo-smue.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Smue/smueWithoutCofactors.xml" -i="paper_data/Smue/inputForSmue.xml" -o -d -fdrr -fp -mP >pitufina-smue.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Smue/smueWithoutCofactors.xml" -i="paper_data/Smue/inputForSmue.xml" -o -d -fdrr -fp -mP -cm >pitufina-min-smue.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Smue/smueWithoutCofactors.xml" -i="paper_data/Smue/inputForSmue.xml" -o -d -fdrr -fp -mM >papaPitufo-smue.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Smue/smueWithoutCofactors.xml" -i="paper_data/Smue/inputForSmue.xml" -o -d -fdrr -fp -mM -cm >papaPitufo-min-smue.out

java -jar -Xmx1000M pitufo.jar -s="paper_data/Buchnera/bucaiRevisedFiltered.xml" -i="paper_data/Buchnera/inputForBuchnera.xml" -o -d -fdrr -fp -mT >pitufo-Buchnera.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Buchnera/bucaiRevisedFiltered.xml" -i="paper_data/Buchnera/inputForBuchnera.xml" -o -d -fdrr -fp -mP >pitufina-Buchnera.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Buchnera/bucaiRevisedFiltered.xml" -i="paper_data/Buchnera/inputForBuchnera.xml" -o -d -fdrr -fp -mP -cm >pitufina-min-Buchnera.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Buchnera/bucaiRevisedFiltered.xml" -i="paper_data/Buchnera/inputForBuchnera.xml" -o -d -fdrr -fp -mM >papaPitufo-Buchnera.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/Buchnera/bucaiRevisedFiltered.xml" -i="paper_data/Buchnera/inputForBuchnera.xml" -o -d -fdrr -fp -mM -cm >papaPitufo-min-Buchnera.out

java -jar -Xmx1000M pitufo.jar -s="paper_data/ecoli/smmWithDirections11.5Corrected.xml" -i="paper_data/ecoli/inputForEcoli.xml" -o -d -fdrr -fp -mT >pitufo-ecoli.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/ecoli/smmWithDirections11.5Corrected.xml" -i="paper_data/ecoli/inputForEcoli.xml" -o -d -fdrr -fp -mP >pitufina-ecoli.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/ecoli/smmWithDirections11.5Corrected.xml" -i="paper_data/ecoli/inputForEcoli.xml" -o -d -fdrr -fp -mP -cm >pitufina-min-ecoli.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/ecoli/smmWithDirections11.5Corrected.xml" -i="paper_data/ecoli/inputForEcoli.xml" -o -d -fdrr -fp -mM >papaPitufo-ecoli.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/ecoli/smmWithDirections11.5Corrected.xml" -i="paper_data/ecoli/inputForEcoli.xml" -o -d -fdrr -fp -mM -cm >papaPitufo-min-ecoli.out

java -jar -Xmx1000M pitufo.jar -s="paper_data/carso/carso.xml" -i="paper_data/carso/inputForCarso.xml" -o -d -fdrr -fp -mT >pitufo-carso.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/carso/carso.xml" -i="paper_data/carso/inputForCarso.xml" -o -d -fdrr -fp -mP >pitufina-carso.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/carso/carso.xml" -i="paper_data/carso/inputForCarso.xml" -o -d -fdrr -fp -mP -cm >pitufina-min-carso.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/carso/carso.xml" -i="paper_data/carso/inputForCarso.xml" -o -d -fdrr -fp -mM >papaPitufo-carso.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/carso/carso.xml" -i="paper_data/carso/inputForCarso.xml" -o -d -fdrr -fp -mM -cm >papaPitufo-min-carso.out

java -jar -Xmx1000M pitufo.jar -s="paper_data/yeast/imm904.xml" -i="paper_data/yeast/inputForYeast.xml" -o -d -fdrr -fp -mT >pitufo-yeast.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/yeast/imm904.xml" -i="paper_data/yeast/inputForYeast.xml" -o -d -fdrr -fp -mP >pitufina-yeast.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/yeast/imm904.xml" -i="paper_data/yeast/inputForYeast.xml" -o -d -fdrr -fp -mP -cm >pitufina-min-yeast.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/yeast/imm904.xml" -i="paper_data/yeast/inputForYeast.xml" -o -d -fdrr -fp -mM >papaPitufo-yeast.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/yeast/imm904.xml" -i="paper_data/yeast/inputForYeast.xml" -o -d -fdrr -fp -mM -cm >papaPitufo-min-yeast.out

java -jar -Xmx1000M pitufo.jar -s="paper_data/human/HUMAN.xml" -i="paper_data/human/inputForHuman.xml" -o -d -fdrr -fp -mT >pitufo-human.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/human/HUMAN.xml" -i="paper_data/human/inputForHuman.xml" -o -d -fdrr -fp -mP >pitufina-human.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/human/HUMAN.xml" -i="paper_data/human/inputForHuman.xml" -o -d -fdrr -fp -mP -cm >pitufina-min-human.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/human/HUMAN.xml" -i="paper_data/human/inputForHuman.xml" -o -d -fdrr -fp -mM >papaPitufo-human.out
java -jar -Xmx1000M pitufo.jar -s="paper_data/human/HUMAN.xml" -i="paper_data/human/inputForHuman.xml" -o -d -fdrr -fp -mM -cm >papaPitufo-min-human.out