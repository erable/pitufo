/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package pitufo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import metabolicNetwork.Compound;
import metabolicNetwork.Reaction;

public class TreeCompoundNode {
	
	String id;
	Boolean isJoker;
	HashMap<String, TreeReactionNode> producingReactionNodes = new HashMap<String, TreeReactionNode>();
	TreeReactionNode usingReactionNode;
	Compound compound;
	Boolean processed;
	Set<String> compoundsVisited;
	Set<String> reactionsVisited;
	Reaction originReaction;
	
	/**
	 * 
	 */
	public TreeCompoundNode(Compound cpd) {
		id = cpd.getId();
		isJoker = false;
		compound = cpd;
		processed = false;
		compoundsVisited = new HashSet<String>();
		reactionsVisited = new HashSet<String>();
	}
	
	public TreeCompoundNode(Compound cpd, String id) {
		this.id = id;
		isJoker = false;
		compound = cpd;
		processed = false;
		compoundsVisited = new HashSet<String>();
		reactionsVisited = new HashSet<String>();
	}
	
	public TreeCompoundNode(TreeCompoundNode ori) {
		id = ori.id;
		isJoker = ori.isJoker;
		compound = ori.compound;
		processed = ori.processed;
		originReaction = ori.originReaction;
	}
	 
	@Override
	public String toString() {
		return "<" + compound.getId() +">";
	}
	
	public Boolean isReady() {
		return isJoker || (processed && producingReactionNodes.size() == 0);
	}
	
	public Boolean isLeaf() {
		return producingReactionNodes.size() == 0;
	}
	
	/**
	 * Minimize when all the reactions which produce this compound are ready, i.e.
	 * when they are complete and when their child nodes are leaves
	 *
	 */
	public void minimize() {
		HashMap<String, TreeReactionNode> reactions = new HashMap<String, TreeReactionNode>();
		reactions.putAll(producingReactionNodes);

		// it is necessary that all reactions are ready
		for(TreeReactionNode reaction : reactions.values()) {
			if(! reaction.isReady() ) {
				return;
			}
		}

		for(TreeReactionNode reaction : reactions.values()) {
			for(TreeReactionNode reaction2 : reactions.values()) {
				if( !reaction.equals(reaction2) ) {
					Set<String> set1 = new HashSet<String>();
					Set<String> set2 = new HashSet<String>();

					for(TreeCompoundNode node : reaction.substrateNodes.values()) {
						if(! node.isJoker ) {
							set1.add(node.id);
						}
					}

					for(TreeCompoundNode node : reaction2.substrateNodes.values()) {
						if(! node.isJoker ) {
							set2.add(node.id);
						}
					}
					
					if(set1.containsAll(set2)) {
						this.producingReactionNodes.remove(reaction);
					}
				}
			}
		}
	}
	

	public TreeReactionNode getUsingReactionNode() {
		return usingReactionNode;
	}

	public void setUsingReactionNode(TreeReactionNode usingReactionNode) {
		this.usingReactionNode = usingReactionNode;
	}

	public void addProducingReactionNode(TreeReactionNode e) {
		producingReactionNodes.put(e.id, e);
	}
	

}
