/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package pitufo;

import java.util.ArrayList;
import java.util.List;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;
import metabolicNetwork.Reaction;
import utils.StringUtils;
import application.InputParameters;
import application.MethodComparison;
import application.PrecursorSet;

public class Pitufo extends PrecursorFinder
{
    
	public Pitufo(MetabolicNetwork network, boolean specialEmptySet)
	{
		super(network, specialEmptySet);
	}
	
	public List<PrecursorSet> findPrecursorWithReplacementTree(boolean ignoreReversibility, MethodComparison mc) {				
		// Calls for the second phase.
		System.out.println("\n\nSearching for precursors IN A REPLACEMENT TREE (Pitufo method)\n");
		
		List<PrecursorSet> missingPrecursors = new ArrayList<PrecursorSet>();
		if( InputParameters.oneByOne ) 
		{
			for(Compound target : InputParameters.getTargetCompounds()) 
			{
				// computes the minimal precursor sets (starts counting time to compute)
				Long start = System.currentTimeMillis();
				
				// finds the copy of the target compound in the restored network
				Compound tc = getNetwork().getCompounds().get(target.getId());
				tc.setTarget(true);
				
				String id = tc.getId();
				if(InputParameters.decodeSbml)
					id = StringUtils.sbmlDecode(id);

				// let only the chosen target be set as target for this run
				for(Compound t : targets) 
				{
					if( t != tc ) {
						t.setTarget(false);
					}
				}				

				getNetwork().extendTopologicalPrecursors(InputParameters.precursorIfProducedOnlyByReversible);
				
				// checks if a direction should be chosen for the reversible reactions
				if ( InputParameters.emptyCyclesCut ) {
					System.out.println("Cutting empty cycles from the target "+tc.getId());									
					getNetwork().removeEmptyCyclesForTarget(tc);
					System.out.println("\nThe reduced network after cutting empty cycles has "+getNetwork().getReactions().size()+" reactions and "+getNetwork().getCompounds().size()+" compounds.");								
				}				
				// removes the "new" topological precursors obtained after reaction removals
				getNetwork().removeTopologicalSourcesThatAreNotPrecursors();
				
				//getNetwork().print();				
				
				System.out.println("Searching for precursors of "+id);
				numRecursions = 0;
				BuildAndReplace bar = new BuildAndReplace(tc);
				missingPrecursors = bar.getSetsOfSolutions();
				
				// registering the results of the method for the target
				if( mc != null ) {
					mc.register(tc, backup, getNetwork(), missingPrecursors, numRecursions, System.currentTimeMillis()-start);
				}
				
				System.out.println("\n--- "+id+" precursors - "+missingPrecursors.size()+" solutions");				
				printSolutions(missingPrecursors, targets);
				System.out.println("Processing of "+id+" finished in "+(System.currentTimeMillis()-start)+" ms.\n--------------------\n");
				// for each target, it should start with a brand-new copy of the network
				restoreNetwork();
			}	
		}
		else 
		{
            // We add an artificial compound "TARGET" and artificial reactions
			// from the targets to this compound to reduce the problem
			// of finding precursors to only one target.
			Long start = System.currentTimeMillis();
			System.out.println("Searching for precursors for the artificial target.");

			Compound target = new Compound("TARGET", "TARGET", "");	
			Reaction newReaction = new Reaction("R_TARGET", "R_TARGET", false);
				
			int n = 0;
			for(Compound t : InputParameters.getTargetCompounds()) 
			{
				n++;
				newReaction.addSubstrate(t);
				t.addSubstrateOf(newReaction);
			}
			newReaction.addProduct(target);
			target.addProducedBy(newReaction);
			getNetwork().getReactions().put(newReaction.getId(), newReaction);

			numRecursions = 0;
			BuildAndReplace bar = new BuildAndReplace(target);
			missingPrecursors = bar.getSetsOfSolutions();

			if( mc != null ) {
				mc.register(target, backup, getNetwork(), missingPrecursors, numRecursions, System.currentTimeMillis()-start);
			}
			
			System.out.println("\n--- TARGET precursors - "+missingPrecursors.size()+" solutions");
			printSolutions(missingPrecursors, InputParameters.getTargetCompounds());
			System.out.println("Processing of the artificial target finished in "+(System.currentTimeMillis()-start)+" ms.\n--------------------\n");
		}
		return missingPrecursors;
	}	
}
