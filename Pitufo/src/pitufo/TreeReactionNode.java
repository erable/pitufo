/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package pitufo;

import java.util.HashMap;

public class TreeReactionNode {
	
	String id;
	HashMap<String, TreeCompoundNode> substrateNodes = new HashMap<String, TreeCompoundNode>();
	HashMap<String, TreeCompoundNode> productNodes   = new HashMap<String, TreeCompoundNode>();;
	
	public TreeReactionNode(TreeReactionNode ori, String str) {
		id = str;
		for(TreeCompoundNode sub : ori.substrateNodes.values()) {
			TreeCompoundNode newCpdNode = new TreeCompoundNode(sub);
			addSubstrateNode(newCpdNode);
		}
	}
	
	public TreeReactionNode(String str) {
		id = str;
	}

	public String toString() {
		String str=id+" : ";
		int n = 0;
		for(TreeCompoundNode node : substrateNodes.values()) {
			if(n!=0)  {
				str = str.concat(" + ");
			}
			str = str.concat(node.toString());
			n++;
		}

		str = str.concat(" -> ");
		n = 0;
		for(TreeCompoundNode node : productNodes.values()) {
			if(n!=0)  {
				str = str.concat(" + ");
			}
			str = str.concat(node.toString());
			n++;
		}
		return str;
	}
	
	public void addSubstrateNode(TreeCompoundNode e) {
		if(substrateNodes.containsKey(e.id) == false) 
			substrateNodes.put(e.id, e);
	}
	
	public void delSubstrateNode(TreeCompoundNode e) {
		substrateNodes.remove(e.id);
		return;
	}
	
	public void delProductNode(TreeCompoundNode e) {
		productNodes.remove(e.id);
		return;
	}
	
	public void delSubstrateNode(String id) {
		substrateNodes.remove(id);
		return;
	}
	
	public void addProductNode(TreeCompoundNode e) {
		if(productNodes.containsKey(e.id) == false) 
			productNodes.put(e.id, e);
	}
	
	public boolean isReady() {
		// a reaction is ready when all its compounds have been build in the tree and
		// when all its compoundsNodes are leaves of the tree.
		for(TreeCompoundNode node : substrateNodes.values()) {
			if(! node.isReady() ) {
				return false;
			}
		}
		return true;
		
	}
	
	public boolean allPrecursors() {
		for(TreeCompoundNode node : substrateNodes.values()) {
			if(!node.compound.isPrecursor()) {
				return false;
			}
		}
		return true;
	}

}
