/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package pitufo;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import metabolicNetwork.Compound;
import metabolicNetwork.Reaction;
import application.PrecursorSet;

public class BuildAndReplace {

	TreeCompoundNode targetNode;
	int nReactions;
	int nCpds;
	
	int nEmpty = 0;
	
	ArrayDeque<TreeCompoundNode> leaves;
	
	public BuildAndReplace(Compound target) 
	{
		leaves = new ArrayDeque<TreeCompoundNode>();
		nReactions = 0;
		nCpds = 0;
		
		build(target);
		replace();
	}
	
	public void build(Compound target) 
	{
		ArrayDeque<TreeCompoundNode> compoundNodesToBuild = new ArrayDeque<TreeCompoundNode>();
		targetNode = new TreeCompoundNode(target, newCpdId());
		compoundNodesToBuild.add(targetNode);
		
		//System.err.println("Building ...");
		while(compoundNodesToBuild.size() != 0) 
		{
			TreeCompoundNode cpdNode = compoundNodesToBuild.peekLast();
	
			if(!cpdNode.isReady() && !cpdNode.processed) 
			{
				Compound cpd = cpdNode.compound;
				// Looks if the compound has already been visited
				if( cpdNode.compoundsVisited.contains(cpdNode.compound.getId()) ) 
				{
					if(cpdNode.compound.isAllowed() && !cpdNode.compound.isPrecursor())  {
						cpdNode.isJoker = true;
					}
				}
				else 
				{
					List<Reaction> producedBy = cpd.getProducedBy();

					for(Reaction rxn : producedBy) 
					{
						if( !rxn.getProduces().containsKey(cpd.getId()))
							System.err.println("Error: took a reaction that do not produce the expected compound.");

						if( !cpdNode.reactionsVisited.contains(rxn.getId()) ) 
						{
							// We compare with the parent reactionNode to disallow to go back by the same reversible reaction
							// System.err.println("rxnProducing : "+rxn);
							TreeReactionNode newReactionNode = new TreeReactionNode(newReactionId());
							newReactionNode.addProductNode(cpdNode);
							cpdNode.addProducingReactionNode(newReactionNode);

							// We create the substrate nodes of the new reaction node
							for(Compound sub : rxn.getSubstrates().values()) {
								TreeCompoundNode subNode = new TreeCompoundNode(sub);
								subNode.id = newCpdId();
								subNode.usingReactionNode = newReactionNode;
								newReactionNode.addSubstrateNode(subNode);
								subNode.compoundsVisited.add(cpd.getId());
								subNode.compoundsVisited.addAll(cpdNode.compoundsVisited);

								subNode.reactionsVisited.addAll(cpdNode.reactionsVisited);
								if( rxn.isReversible() ) {
									subNode.reactionsVisited.add(rxn.getId());
									subNode.reactionsVisited.add(rxn.getReverseReaction().getId());
								}

								// We add in the visited set also the co-substrates 
								// (if they are not source compounds) of rxn
								for(Compound sub2 : rxn.getSubstrates().values()) 
								{
									List<Reaction> sub2ProducedBy = sub2.getProducedBy(); 
									
									if(!sub2.getId().equals(sub.getId()) && sub2ProducedBy.size() > 0) {
										subNode.compoundsVisited.add(sub2.getId());
									}
								}

								compoundNodesToBuild.add(subNode);
							}
						}
					}
				}

			}
			cpdNode.processed = true;
			compoundNodesToBuild.remove(cpdNode);
			if(cpdNode.producingReactionNodes.size()==0) {
				leaves.add(cpdNode);
			}
			cpdNode.reactionsVisited.clear();
			cpdNode.reactionsVisited=null;
			cpdNode.compoundsVisited.clear();
			cpdNode.compoundsVisited=null;
		}
	}
	
	public void replace() {
		//System.err.println("Replacing...");
		
		while(leaves.size() != 0) {
			TreeCompoundNode cpdNode = leaves.peekLast();
			TreeReactionNode RU = cpdNode.usingReactionNode;
			if(RU != null) {
				Boolean toRemove = false;
				for(TreeCompoundNode node : RU.substrateNodes.values()) {
					// strange case!! A "leaf" reaction node which has a cpd which is no source nor joker and yet there is no reaction producing it..!?!
					if(node.producingReactionNodes.size() == 0 && !node.compound.isPrecursor() && !node.isJoker ) {	
						toRemove=true;
						break;
					}
				}
				
				if(toRemove) {
					//System.out.println("ALERT: strange case!! Is no source nor joker and yet there is no reaction producing it..!?!");
					removeNode(RU);
				}
				else {
					// a reaction is ready if all its compounds are source or jokers
					if( RU.isReady() ) {  
						HashMap<String, TreeCompoundNode> products = new HashMap<String, TreeCompoundNode>();
						products.putAll(RU.productNodes);

						// for each product of RU
						for(TreeCompoundNode product : products.values()) {
							// The reaction using cpd is ready, i.e. all its child nodes are leaves
							// and has been completed

							// We compare the substrates of the reactions with the ready reactions producing the target
							HashMap<String, TreeReactionNode> RPPs = product.producingReactionNodes;

							// if for this product, RU is not minimal, then remove it from the list of producing reactions for the product.
							if(toRemove(RU, RPPs)) {
								product.producingReactionNodes.remove(RU.id);
								RU.productNodes.remove(product.id);
								if(product.producingReactionNodes.size()== 0 && !leaves.contains(product.id)) {
									leaves.add(product);
									product.processed=true;
								}
							}
							else {
								TreeReactionNode RUP = product.usingReactionNode;

								if(RUP != null) {
									if(RPPs.size() == 1) {
										if(product.compound.isPrecursor()) {
											// We duplicate RUP
											compressReactions(RU, RUP, product);
											if( !leaves.contains(product) ) {
												leaves.add(product);
											}
											product.processed = true;
										}
										else {
											compressReactions(RU, RUP, product);
											removeNode(RUP);
										}
									}
									else {
										compressReactions(RU, RUP, product);
									}
								}
							}
						}
						if(RU.productNodes.size()==0) {
							removeNode(RU);
						}
					}
				}
			}
			if(cpdNode != null) {
				leaves.remove(cpdNode);
			}

		}
		
		// eliminating the non-minimal??
		Set<TreeReactionNode> reactions = new HashSet<TreeReactionNode>();
		reactions.addAll(targetNode.producingReactionNodes.values());
		
		boolean changed = true;
		while(changed) {
			changed = false;
			for(TreeReactionNode reaction : reactions) {
				if(targetNode.producingReactionNodes.containsValue(reaction)) {
					if(toRemove(reaction, targetNode.producingReactionNodes)) {
						removeNode(reaction);
						changed = true;
					}
				}
			}
		}
		
	}
	
	private String newReactionId() {
		nReactions++;
		return "R"+nReactions;
	}
	
	private String newCpdId() {
		nCpds++;
		return "C"+nCpds;
	}
	
	private void compressReactions(TreeReactionNode bottom, TreeReactionNode top, TreeCompoundNode nodeToReplace) {
		TreeReactionNode newReactionNode = new TreeReactionNode(newReactionId());
		
		boolean insertedAsLeaf = false;
		// we copy the substrates, without the compound to replace, of the top reaction in the new reaction 
		for(TreeCompoundNode sub : top.substrateNodes.values()) {
			if(! sub.equals(nodeToReplace) ) {
				TreeCompoundNode newNode = new TreeCompoundNode(sub);
				newNode.id = newCpdId();
				newReactionNode.addSubstrateNode(newNode);
				newNode.usingReactionNode = newReactionNode;
				
				for(TreeReactionNode reactionNode : sub.producingReactionNodes.values()) {
					newNode.addProducingReactionNode(reactionNode);
					reactionNode.addProductNode(newNode);
				}
				
				if(leaves.contains(sub)) {
					leaves.add(newNode);
					insertedAsLeaf = true;
				}
			}
		}
		
		// We copy the substrates of the bottom reaction in the new reaction
		for(TreeCompoundNode sub : bottom.substrateNodes.values()) {

			boolean repeated = false;
			// avoid to include a substrate which is already present (or which is the nodeToReplace)
			if( sub.isReady() ) {
				for(TreeCompoundNode node : newReactionNode.substrateNodes.values()) {
					if( sub.compound.getId().equals(node.compound.getId()) && sub.isJoker == node.isJoker ) {
						repeated = true;
						break;
					}
				}			
			}
			
			if( repeated )
				continue;
			
			TreeCompoundNode newNode = new TreeCompoundNode(sub);
			newNode.id = newCpdId();
			newReactionNode.addSubstrateNode(newNode);
			newNode.usingReactionNode = newReactionNode;

			// Only one compound more in compoundNodeToComplete is enough to do the compression
			// after
			if(!insertedAsLeaf) {
				leaves.add(newNode);
				insertedAsLeaf = true;
			}
		}
		
		if(!insertedAsLeaf) {
			TreeCompoundNode n = newReactionNode.substrateNodes.values().iterator().next();
			leaves.add(n);
		}
		newReactionNode.productNodes = new HashMap<String, TreeCompoundNode>();
		newReactionNode.productNodes.putAll(top.productNodes);
		for(TreeCompoundNode product : newReactionNode.productNodes.values()) {
			product.addProducingReactionNode(newReactionNode);
		}
		
		bottom.delProductNode(nodeToReplace);
		nodeToReplace.producingReactionNodes.remove(bottom.id);
		if(bottom.productNodes.size()==0) 
			removeNode(bottom);
		
		HashMap<String, TreeCompoundNode> products = new HashMap<String, TreeCompoundNode>();
		products.putAll(newReactionNode.productNodes);
		
		for(TreeCompoundNode product : products.values()) {
			HashMap<String, TreeReactionNode> RPPs = product.producingReactionNodes;

			if(toRemove(newReactionNode, RPPs)) {
				product.producingReactionNodes.remove(newReactionNode.id);
				newReactionNode.productNodes.remove(product.id);
				if(product.producingReactionNodes.size()== 0 && !leaves.contains(product)) {
					leaves.add(product);
					product.processed=true;
				}
			}
		}
		if(newReactionNode.productNodes.size()==0)
			removeNode(newReactionNode);
	}
	
	public void removeNode(TreeReactionNode reaction) {
		for(TreeCompoundNode node : reaction.substrateNodes.values()) {
			leaves.remove(node);
			node = null;
		}
		
		for(TreeCompoundNode product : reaction.productNodes.values()) {
			product.producingReactionNodes.remove(reaction.id);
			if(product.producingReactionNodes.size()== 0 && !leaves.contains(product)) {
				leaves.add(product);
				product.processed=true;
			}
		}
		
		reaction = null;
	}
	
	private boolean checkIfSubstrateSetHasLeafWhichIsNoPrecursorNorJoker(TreeReactionNode reaction) {
		for(TreeCompoundNode node : reaction.substrateNodes.values()) {
			// strange case!! A "leaf" reaction node which has a cpd which is no source nor joker and yet there is no reaction producing it..!?!
			if(node.producingReactionNodes.size() == 0 && !node.compound.isPrecursor() && !node.isJoker ) {	
				return true;
			}
		}		
		return false;
	}
	
	/**
	 * Test if the reaction is good and if it's minimal
	 * @param reactionTest
	 * @param reactionsToTest
	 * @return
	 */
	public Boolean toRemove(TreeReactionNode reactionTest, HashMap<String, TreeReactionNode> reactionsToTest) {
		if( checkIfSubstrateSetHasLeafWhichIsNoPrecursorNorJoker(reactionTest) )
			return true;
		
		// put in set1 all the ids of non joker substrates in the reaction to test
		Set<String> set1 = new HashSet<String>();
		for(TreeCompoundNode node : reactionTest.substrateNodes.values()) {
			if(! node.isJoker  ) {
				set1.add(node.compound.getId());
			}
		}
		
		// then, for each reaction of the set to test
		for(TreeReactionNode reaction : reactionsToTest.values()) {
			if( !reaction.id.equals(reactionTest.id) && !checkIfSubstrateSetHasLeafWhichIsNoPrecursorNorJoker(reaction)) {
				// set2 contains all ids of non joker substrates of reaction
				Set<String> set2 = new HashSet<String>();
				for(TreeCompoundNode node : reaction.substrateNodes.values()) {
					if( !node.isJoker ) {
						set2.add(node.compound.getId() );
					}
				}

				// if found a reaction which is a subset of the reaction to test r1, then r1 is not minimal
				if(set2.size() > 0 && set1.containsAll(set2)) {
					return true;
				}
				
				// if both reaction to test and some other are "empty" (only jokers)
				if(set1.size() == 0 && set2.size() == 0) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public List<PrecursorSet> getSetsOfSolutions() {
		
		List<PrecursorSet> solutions = new ArrayList<PrecursorSet>();
			
		for(TreeReactionNode reactionNode : targetNode.producingReactionNodes.values()) {
			if(! reactionNode.isReady()) {
				System.out.println(reactionNode+" not ready !");
				continue; //System.exit(1)
			}
			PrecursorSet sol = new PrecursorSet();
			
			for(TreeCompoundNode cpd : reactionNode.substrateNodes.values()) {
				if(cpd.isJoker) {
					sol.addBootstrap(cpd.compound);
				}
				else {
					sol.addPrecursor(cpd.compound);
				}
			}
			solutions.add(sol);
		}
		return solutions;
	}
	
	
}
