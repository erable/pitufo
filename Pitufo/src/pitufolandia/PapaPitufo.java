/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package pitufolandia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;
import metabolicNetwork.Reaction;
import pitufo.PrecursorFinder;
import application.InputParameters;
import application.PrecursorSet;

public class PapaPitufo extends PrecursorFinder
{
	int numberOfShortcuts = 0;
    int reactionIdSeed = 1;
    
    boolean abort = false;
    
    Reaction toDebug;
	
	public PapaPitufo(MetabolicNetwork network, boolean specialEmptySet)
	{
		super(network, specialEmptySet);
	}

	// Method to find the set of minimal precursor sets that produce a target compound
	// directly in the metabolic network
	public List<PrecursorSet> findPrecursorsInNetworkForTarget(List<Compound> targets, boolean minimalityCheck)
	{
		// Create an special reaction that takes as substrates the targets passed 
		// and that produces an special, TARGET, compound.
		Compound target = createArtificialTargetCompound(targets);		

		// initializes the side compounds with the products for all reactions 
		getNetwork().initSideCompounds();
				
		// Creates the set of the available compounds
		HashMap<String, Compound> A = new HashMap<String, Compound>();

		// Creates the set of results
		List<PrecursorSet> precursors = new ArrayList<PrecursorSet>();

		System.out.println("PAPA PITUFO PAPER (roots!!) method");
		// Find the precursors of the special new target compound
		numberOfShortcuts = 0;
		numRecursions = 0;
		abort = false;
		compactInNetworkFrom(target, A, minimalityCheck);
		if( abort )
			return null;

		boolean emptyAsSolution = false;
		for( Reaction r: target.getReactionsThatProduce(false) )
		{
			PrecursorSet s = new PrecursorSet();
				
			if( r.getSubstrates().isEmpty() && !emptyAsSolution )
			{
				emptyAsSolution = true;
				precursors.add(s);
				// for the "empty set" solution, at least identifies the empty cycle (reactions)
				// that are producing the target
				s.setReactions(r.getCompressedReactions());
			}
			else
			{
				for( Compound c: r.getSubstrates().values() )
				{
					if( c.isPrecursor() )
						s.addPrecursor(c);
					else
						s.addBootstrap(c);
				}
				if( s.getPrecursors().size() > 0 )
					precursors.add(s);
			}
		}
		System.out.println("No. of Shortcuts: "+numberOfShortcuts);
		return reduceToMinimalPrecursorSets(precursors);	
	}
	
	public void compactInNetworkFrom(Compound a, HashMap<String, Compound> A, boolean minimalityCheck)
	{
		numRecursions++;
		
		// if there is a time limit defined, check it.
		if( abort || (InputParameters.maxTimePerTarget > 0 && (System.currentTimeMillis()-timeStart) >= InputParameters.maxTimePerTarget)) {
			abort = true;
			return;
		}
		
		List<Reaction> reactions = findMinimalReactionsThatProduce(a, A);
		Reaction r= null;
		if( minimalityCheck )
		{
			//reactions = findMinimalReactionsThatProduce(a, A); // Selection of reaction
			r = findNewMinimalReactionThatProduces(a, A);
		}
		else 
		{
			//reactions = new ArrayList<Reaction>();
			//reactions.addAll(a.getProducedBy()); // All the reactions
			r = findReactionThatProduces(a, A, false);  // All the reactions
		}
		
		//for(Reaction r: reactions)
		while( r != null )
		{
			System.out.println(numRecursions+") Target="+a.getId()+" - #R = "+reactions.size()+". r="+r);			
			HashMap<String, Compound> newA = new HashMap<String, Compound>(A);
			for(Compound c: r.getProduces().values()) {
				if( !newA.containsKey(c.getId())) {
					newA.put(c.getId(), c);
				}
			}
			for(Compound c: r.getSideCompounds().values()) {
				if( !newA.containsKey(c.getId())) {
					newA.put(c.getId(), c);
				}
			}			
			for(Compound c: r.getSubstrates().values()) {
				if( !newA.containsKey(c.getId())) {
					newA.put(c.getId(), c);
				}
			}			

			List<ReactionSet> reactionsPerSubstrate = new ArrayList<ReactionSet>();
			for( Compound c: r.getSubstrates().values() )
			{
				if( !A.containsKey(c.getId()) && !c.isPrecursor() ) {
					compactInNetworkFrom(c, newA, minimalityCheck);
					ReactionSet setC = new ReactionSet();
					setC.setReactions(findMinimalReactionsThatProduce(c, new HashMap<String, Compound>()));
					reactionsPerSubstrate.add(setC);
				}
			}
			
			List<ReactionSet> RMin = new ArrayList<ReactionSet>();
			// initializes RMin with an empty reaction set
			RMin.add(new ReactionSet());
			// cartesian union of all tuples
			for(ReactionSet reactionSet: reactionsPerSubstrate) {
				List<ReactionSet> singletons = new ArrayList<ReactionSet>();
				for(Reaction reaction: reactionSet.getReactions()) {
					ReactionSet rs = new ReactionSet();
					rs.getReactions().add(reaction);
					singletons.add(rs);
				}
				RMin = reactionSetCartesianUnion(RMin, singletons);
			}
			reduceToMinimalReactionSets(RMin);

			for(ReactionSet rmin: RMin) {
				createShortcut(r, rmin);
			}
			getNetwork().removeReaction(r);
			
			if( minimalityCheck ) {
				r = findNewMinimalReactionThatProduces(a, A);  // Selection of reaction
			}
			else {
				r = findReactionThatProduces(a, A, false);  // All the reactions
			}			
		}
		System.out.println("Target "+a.getId()+" finished.");
	}

	public Reaction createShortcut(Reaction r, ReactionSet rs)
	{
		Integer reactionId = reactionIdSeed++;
		
		Reaction newReaction = getNetwork().addNewReaction(reactionId.toString(), "shortcut"+reactionId.toString(), false);
		
		newReaction.addProducts(r.getProduces().values());
		if( InputParameters.mergeKeepingSideCompounds ) {
			for(Reaction rOther: rs.getReactions()) {
				// add all the previous side compounds
				for(Compound side: rOther.getSideCompounds().values()) {
					if( !newReaction.getSideCompounds().containsKey(side.getId()) )
						newReaction.getSideCompounds().put(side.getId(), side);			
				}
				for(Compound side: rOther.getSideCompounds().values()) {
					if( !newReaction.getSideCompounds().containsKey(side.getId()) )
						newReaction.getSideCompounds().put(side.getId(), side);			
				}					
			}
			// include side compounds
			for( Compound side: r.getSideCompounds().values() )
			{
				if( !newReaction.getSideCompounds().containsKey(side.getId()) )
					newReaction.getSideCompounds().put(side.getId(), side);
			}		
		
		}
		
		// and finally the substrates of each merged reaction
		for(Compound c: r.getSubstrates().values()){			
			if( !newReaction.getProduces().containsKey(c.getId()) && !newReaction.getSideCompounds().containsKey(c.getId()) )
				newReaction.addSubstrate(c);
		}

		for(Reaction rOther: rs.getReactions()) {
			for(Compound c: rOther.getSubstrates().values()){
				if( !newReaction.getProduces().containsKey(c.getId()) && !newReaction.getSideCompounds().containsKey(c.getId()) && !newReaction.getSubstrates().containsKey(c.getId()))
				{
					newReaction.addSubstrate(c);
				}
			}
		}
		numberOfShortcuts++;
		return newReaction;
	}
	
}
