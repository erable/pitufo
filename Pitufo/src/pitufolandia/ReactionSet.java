package pitufolandia;

import java.util.ArrayList;
import java.util.List;

import metabolicNetwork.Reaction;

public class ReactionSet {

	private List<Reaction> reactions = new ArrayList<Reaction>();
	boolean flag = false;
	
	public ReactionSet() {
		
	}
	
	public ReactionSet(ReactionSet r) {
		add(r);
	}	
	
	public List<Reaction> getReactions() {
		return reactions;
	}
	
	public void setReactions(List<Reaction> reactions) {
		this.reactions = reactions;
	}

	public void add(ReactionSet r) {
		reactions.addAll(r.getReactions());
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
}
