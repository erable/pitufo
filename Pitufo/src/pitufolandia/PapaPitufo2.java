/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package pitufolandia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;
import metabolicNetwork.Reaction;
import pitufo.PrecursorFinder;
import application.InputParameters;
import application.PrecursorSet;

public class PapaPitufo2 extends PrecursorFinder
{
	int numberOfShortcuts = 0;
    int reactionIdSeed = 1;
    
    boolean abort = false;
    
    Reaction toDebug;
	
	public PapaPitufo2(MetabolicNetwork network, boolean specialEmptySet)
	{
		super(network, specialEmptySet);
	}

	// Method to find the set of minimal precursor sets that produce a target compound
	// directly in the metabolic network
	public List<PrecursorSet> findPrecursorsInNetworkForTarget(List<Compound> targets, boolean minimalityCheck)
	{
		// Create an special reaction that takes as substrates the targets passed 
		// and that produces an special, TARGET, compound.
		Compound target = createArtificialTargetCompound(targets);

		// initializes the side compounds with the products for all reactions 
		getNetwork().initSideCompounds();
				
		// Creates the set of the available compounds
		HashMap<String, Compound> A = new HashMap<String, Compound>();

		// Creates the set of results
		List<PrecursorSet> precursors = new ArrayList<PrecursorSet>();

		System.out.println("PAPA PITUFO PAPER 2 method");
		// Find the precursors of the special new target compound
		numberOfShortcuts = 0;
		numRecursions = 0;
		abort = false;
		Reaction rTarget = getNetwork().getReactions().get("SpecialReactionThatProducesTargetMinimal");
		
		NS(rTarget, A);
		if( abort )
			return null;

		boolean emptyAsSolution = false;
		for( Reaction r: target.getReactionsThatProduce(false) )
		{
			PrecursorSet s = new PrecursorSet();
				
			if( r.getSubstrates().isEmpty() && !emptyAsSolution )
			{
				emptyAsSolution = true;
				precursors.add(s);
				// for the "empty set" solution, at least identifies the empty cycle (reactions)
				// that are producing the target
				s.setReactions(r.getCompressedReactions());
			}
			else
			{
				for( Compound c: r.getSubstrates().values() )
				{
					if( c.isPrecursor() )
						s.addPrecursor(c);
					else
						s.addBootstrap(c);
				}
				if( s.getPrecursors().size() > 0 )
					precursors.add(s);
			}
		}
		System.out.println("No. of Shortcuts: "+numberOfShortcuts);
		return reduceToMinimalPrecursorSets(precursors);	
	}
	
	public void NS(Reaction r0, HashMap<String, Compound> A)
	{
		numRecursions++;
		
		// if there is a time limit defined, check it.
		if( abort || (InputParameters.maxTimePerTarget > 0 && (System.currentTimeMillis()-timeStart) >= InputParameters.maxTimePerTarget)) {
			abort = true;
			return;
		}

		List<Compound> M = new ArrayList<Compound>();
		if( choosePivot(r0, A) != null ) {
			for(Compound m: r0.getSubstrates().values()) {
				if( !m.isPrecursor() && !A.containsKey(m.getId())) {
					M.add(m);
					HashMap<String, Compound> newA = new HashMap<String, Compound>(A);
					for(Compound c: r0.getSubstrates().values()) {
						if( !newA.containsKey(c.getId()) && !c.getId().equals(m.getId())) {
							newA.put(c.getId(), c);
						}
					}						
					
					List<Reaction> reactions = findMinimalReactionsThatProduce(m, newA);
					for(Reaction r: reactions) {
						HashMap<String, Compound> newAWithSide = new HashMap<String, Compound>(newA);
						for(Compound c: r.getSideCompounds().values()) {
							if( !newAWithSide.containsKey(c.getId())) {
								newAWithSide.put(c.getId(), c);
							}
						}							
						NS(r, newAWithSide);
					}
				}
			}
			
			Replace(r0, M);
		}
	}
		
	public void Replace(Reaction r, List<Compound> M) {
			List<ReactionSet> reactionsPerSubstrate = new ArrayList<ReactionSet>();
			for( Compound m: M )
			{
				ReactionSet setC = new ReactionSet();
				setC.setReactions(findMinimalReactionsThatProduce(m, new HashMap<String, Compound>()));
				reactionsPerSubstrate.add(setC);
			}
			List<ReactionSet> RMin = new ArrayList<ReactionSet>();
			// initializes RMin with an empty reaction set
			RMin.add(new ReactionSet());
			// cartesian union of all tuples
			for(ReactionSet reactionSet: reactionsPerSubstrate) {
				List<ReactionSet> singletons = new ArrayList<ReactionSet>();
				for(Reaction reaction: reactionSet.getReactions()) {
					ReactionSet rs = new ReactionSet();
					rs.getReactions().add(reaction);
					singletons.add(rs);
				}
				RMin = reactionSetCartesianUnion(RMin, singletons);
			}
			reduceToMinimalReactionSets(RMin);

			for(ReactionSet rmin: RMin) {
				createShortcut(r, rmin);
			}
			getNetwork().removeReaction(r);
	}

	public Reaction createShortcut(Reaction r, ReactionSet rs)
	{
		Integer reactionId = reactionIdSeed++;
		
		Reaction newReaction = getNetwork().addNewReaction(reactionId.toString(), "shortcut"+reactionId.toString(), false);
		
		newReaction.addProducts(r.getProduces().values());
		if( InputParameters.mergeKeepingSideCompounds ) {
			for(Reaction rOther: rs.getReactions()) {
				// add all the previous side compounds
				for(Compound side: rOther.getSideCompounds().values()) {
					if( !newReaction.getSideCompounds().containsKey(side.getId()) )
						newReaction.getSideCompounds().put(side.getId(), side);			
				}
				for(Compound side: rOther.getSideCompounds().values()) {
					if( !newReaction.getSideCompounds().containsKey(side.getId()) )
						newReaction.getSideCompounds().put(side.getId(), side);			
				}					
			}
			// include side compounds
			for( Compound side: r.getSideCompounds().values() )
			{
				if( !newReaction.getSideCompounds().containsKey(side.getId()) )
					newReaction.getSideCompounds().put(side.getId(), side);
			}		
		
		}
		
		// and finally the substrates of each merged reaction
		for(Compound c: r.getSubstrates().values()){			
			if( !newReaction.getProduces().containsKey(c.getId()) && !newReaction.getSideCompounds().containsKey(c.getId()) )
				newReaction.addSubstrate(c);
		}

		for(Reaction rOther: rs.getReactions()) {
			for(Compound c: rOther.getSubstrates().values()){
				if( !newReaction.getProduces().containsKey(c.getId()) && !newReaction.getSideCompounds().containsKey(c.getId()) && !newReaction.getSubstrates().containsKey(c.getId()))
				{
					newReaction.addSubstrate(c);
				}
			}
		}
		numberOfShortcuts++;
		return newReaction;
	}
	
}
