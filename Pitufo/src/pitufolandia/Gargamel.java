/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package pitufolandia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;
import metabolicNetwork.Reaction;
import pitufo.PrecursorFinder;
import application.InputParameters;
import application.PrecursorSet;

public class Gargamel extends PrecursorFinder
{
    
	public boolean abort = false;
	
	public Gargamel(MetabolicNetwork network, boolean specialEmptySet)
	{
		super(network, specialEmptySet);
	}
	
	// Method to find the set of minimal precursor sets that produce a target compound
	// directly in the metabolic network
	public List<PrecursorSet> findPrecursorsInNetworkForTarget(List<Compound> targets, boolean minimalityCheck)
	{
		// Create an special reaction that takes as substrates the targets passed 
		// and that produces an special, TARGET, compound.
		Compound target = createArtificialTargetCompound(targets);
		
		// Creates the set of the available compounds
		HashMap<String, Compound> A = new HashMap<String, Compound>();

		// Creates the set of results
//		List<PrecursorSet> precursors = new ArrayList<PrecursorSet>();

		System.out.println("GARGAMEL method");
		numRecursions = 0;
		abort = false;
		List<PrecursorSet> solutions = minSourcesHyperpaths(target, A, minimalityCheck);//, InputParameters.getTargetCompounds());
		if( abort )
			return null;
		return solutions;
	}

	public List<PrecursorSet> minSourcesHyperpaths(Compound a, HashMap<String, Compound> A, boolean minimalityCheck)
	{
		numRecursions++;
		//System.out.println("> Compound "+a.getId());//+" Hk={"+Hk+"}");
		List<PrecursorSet> P = new ArrayList<PrecursorSet>();
		
		// if there is a time limit definition, check it.
		if( abort || (InputParameters.maxTimePerTarget > 0 && (System.currentTimeMillis()-timeStart) >= InputParameters.maxTimePerTarget) ) {
			abort = true;
			return P;
		}
		
		// if a is a source
		if( a.isPrecursor() )
		{
			PrecursorSet s = new PrecursorSet();
			s.addPrecursor(a);
			P.add(s);
			return P;
		}
		
		List<PrecursorSet> Pr = new ArrayList<PrecursorSet>();
		Queue<Reaction> reactionsToAnalyze = new LinkedList<Reaction>(); 
		// this set is only used to make it easier to compute the cartesian union
		PrecursorSet emptySet = new PrecursorSet();
		emptySet.setEmptyCompounds(true);
		
		if( minimalityCheck ) {
			reactionsToAnalyze.addAll( findMinimalReactionsThatProduce(a, A) );
		}
		else 
		{
			reactionsToAnalyze.addAll(a.getProducedBy());
		}		
		
		while( ! reactionsToAnalyze.isEmpty() ) 
		{
			if( a.getId().equals("C") )
				System.out.println("Debug...");
			
			Reaction r = reactionsToAnalyze.poll();
			HashMap<String, Compound> newA = new HashMap<String, Compound>(A);
			if( !newA.containsKey(a.getId())) {
					newA.put(a.getId(), a);
			}

			Pr = new ArrayList<PrecursorSet>();
			Pr.add( emptySet );	// Initializes with a set that contains only the empty set as solution
			
			HashMap<String, Compound> backupNewA = new HashMap<String, Compound>(newA);
			for( Compound c: r.getSubstrates().values() )
			{
				if( !A.containsKey(c.getId()) )
				{
					Pr = precursorSetCartesianUnion(Pr, minSourcesHyperpaths(c, newA, minimalityCheck));
					newA = new HashMap<String, Compound>(backupNewA);
				}
				else	// cycle identified
				{
					Compound emptyC = createEmptyProducer(c);
					PrecursorSet s = new PrecursorSet();
					s.setEmptyCompounds(true);
					s.addPrecursor(emptyC);
					
					List<PrecursorSet> solEmptyC = new ArrayList<PrecursorSet>();
					solEmptyC.add(s);
					
					Pr = precursorSetCartesianUnion(Pr, solEmptyC);
				}
			}

			// cleaning from Pr invalid cycles
			cleanSetsThatContainsEmptyA(Pr, a);
			
			P.addAll(Pr);
		}
		//System.out.println("! Visited all paths, returning.");
		return reduceToMinimalPrecursorSets(P);
	}

	public List<PrecursorSet> precursorSetCartesianUnion(List<PrecursorSet> l1, List<PrecursorSet> l2)
	{
		List<PrecursorSet> c = new ArrayList<PrecursorSet>();
		for(PrecursorSet s1: l1)
		{
			for(PrecursorSet s2: l2)
			{
				// check if the first element of the set is not an empty compound
				if( s1.isEmptyCompounds() == s2.isEmptyCompounds() ) {
					PrecursorSet s = new PrecursorSet(s1);
					s.setEmptyCompounds(s1.isEmptyCompounds());
					s.add(s2);
					c.add(s);
				}
				else {
					if( !s1.isEmptyCompounds() ) {
						c.add(s1);
					}
					else {
						c.add(s2);
					}
				}
			}
		}
		return c;
	}	
	
	private void cleanSetsThatContainsEmptyA(List<PrecursorSet> precursorSets, Compound a) {
		for(int i = precursorSets.size()-1; i >= 0; i--) {
			if( precursorSets.get(i).isEmptyCompounds() ) {
				for(Compound c: precursorSets.get(i).getPrecursors()) {
					if( c.isEmptyCompound() && c.getCycledCompound().equals(a) ) {
						precursorSets.remove(i);
						break;
					}
				}
			}
		}
	}
}
