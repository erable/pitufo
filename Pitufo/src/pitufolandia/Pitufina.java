/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package pitufolandia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;
import metabolicNetwork.Reaction;
import pitufo.PrecursorFinder;
import application.InputParameters;
import application.PrecursorSet;

public class Pitufina extends PrecursorFinder
{
    
	public boolean abort = false;
	
	public Pitufina(MetabolicNetwork network, boolean specialEmptySet)
	{
		super(network, specialEmptySet);
	}
	
	// Method to find the set of minimal precursor sets that produce a target compound
	// directly in the metabolic network
	public List<PrecursorSet> findPrecursorsInNetworkForTarget(List<Compound> targets, boolean minimalityCheck)
	{
		// Create an special reaction that takes as substrates the targets passed 
		// and that produces an special, TARGET, compound.
		Compound target = createArtificialTargetCompound(targets);
		
		// Creates the set of the available compounds
		HashMap<String, Compound> Hk = new HashMap<String, Compound>();

		// Creates the set of results
//		List<PrecursorSet> precursors = new ArrayList<PrecursorSet>();

		System.out.println("PITUFINA method");
		numRecursions = 0;
		abort = false;
		List<PrecursorSet> solutions = minSourcesHyperpaths(target, Hk, minimalityCheck);//, InputParameters.getTargetCompounds());
		if( abort )
			return null;
		return solutions;
	}

	public List<PrecursorSet> minSourcesHyperpaths(Compound a, HashMap<String, Compound> Hk, boolean minimalityCheck)
	{
		numRecursions++;
		//System.out.println("> Compound "+a.getId());//+" Hk={"+Hk+"}");
		List<PrecursorSet> P = new ArrayList<PrecursorSet>();
		if( abort || (InputParameters.maxTimePerTarget > 0 && (System.currentTimeMillis()-timeStart) >= InputParameters.maxTimePerTarget) ) {
			abort = true;
			return P;
		}
		
		if( a.isPrecursor() )
		{
			PrecursorSet s = new PrecursorSet();
			s.addPrecursor(a);
			P.add(s);
			return P;
		}
		List<PrecursorSet> Pr = new ArrayList<PrecursorSet>();
		
		Queue<Reaction> reactionsToAnalyze = new LinkedList<Reaction>(); 
		
		if( minimalityCheck ) {
			reactionsToAnalyze.addAll( findMinimalReactionsThatProduce(a, Hk) );
		}
		else 
		{
			reactionsToAnalyze.addAll(a.getProducedBy());
		}
		
		while( ! reactionsToAnalyze.isEmpty() ) 
		{
			Reaction r = reactionsToAnalyze.poll();
			HashMap<String, Compound> NewHk = new HashMap<String, Compound>(Hk);
			for(Compound c: r.getProduces().values())
			{
				if( !NewHk.containsKey(c.getId()))
					NewHk.put(c.getId(), c);
			}
			for(Compound c: r.getSubstrates().values())
			{
				if( !NewHk.containsKey(c.getId()))
					NewHk.put(c.getId(), c);
			}

			Pr = new ArrayList<PrecursorSet>();
			Pr.add( new PrecursorSet() );	// Initializes with a set that contains only the empty set as solution
			
			HashMap<String, Compound> backupNewHk = new HashMap<String, Compound>(NewHk);
			for( Compound c: r.getSubstrates().values() )
			{
				if( !Hk.containsKey(c.getId()) )
				{
					Pr = precursorSetCartesianUnion(Pr, minSourcesHyperpaths(c, NewHk, minimalityCheck));
					NewHk = new HashMap<String, Compound>(backupNewHk);
				}
				
				// NEW!!! Caution.
				// New attempt to take into account intermediate "empty set" producers by replacing the empty set by a special reaction
				// that directly produces the compound of interest
				if( specialEmptySet && containsEmptySet(Pr) )
					transformEmptySetForTarget(Pr, c);
				
			}
			P.addAll(Pr);
		}
		//System.out.println("! Visited all paths, returning.");
		return reduceToMinimalPrecursorSets(P);
	}
		
}
