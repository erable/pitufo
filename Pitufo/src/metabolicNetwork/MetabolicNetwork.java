/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package metabolicNetwork;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;

import application.InputParameters;
import application.MaximalTarget;

public class MetabolicNetwork {
	
    // Compounds and reactions definition    
	HashMap<String, Compound> compounds = new HashMap<String, Compound>();
	HashMap<String,Reaction>  reactions = new HashMap<String, Reaction>();

	public MetabolicNetwork hardCopy() {
		MetabolicNetwork copy = new MetabolicNetwork();
		// copy the compounds
		for(Compound c: compounds.values()) {
			Compound copiedCompound = copy.addCompound(c.getId(), c.getName(), c.compartment);
			copiedCompound.copyPropertiesFrom(c);
		}
		
		// copy the reactions
		for(Reaction r: reactions.values()) {
			if( r.isReversible() && r.getId().endsWith("_REV") )
				continue;
			
			Reaction copiedReaction = copy.addNewReaction(r.getId(), r.getName(), r.isReversible());
			Reaction r2 = null;
            if( r.isReversible() ) {
            	r2 = copy.addNewReaction(r.getId()+"_REV", r.getName()+"_REV", r.isReversible());
            	copiedReaction.setReverse(r2);
            }
            
            // add the substrates of the reaction
            for(Compound substrate: r.getSubstrates().values()) {
	            // Finds the compound
            	String cId = substrate.getId();
            	Compound c = copy.getCompounds().get( cId );
	            if( c != null)
	            {
	            	copiedReaction.addSubstrate(c);
	            	if( r.isReversible() )
	            		r2.addProduct(c);
	            }
            }

            // add the products of the reaction
            for(Compound product: r.getProduces().values()) {
	            // Finds the compound
            	String cId = product.getId();
	            Compound c = copy.getCompounds().get( cId );
	            if( c != null)
	            {
	            	copiedReaction.addProduct(c);
	            	if( r.isReversible() )
	            		r2.addSubstrate(c);
	            }
            }        
		}
		
		return copy;
	}
	
	public void print(){
		for (Iterator<Compound> iter = compounds.values().iterator(); iter.hasNext();) 
		{
			Compound c = (Compound) iter.next();
			System.out.println(c.id);
			
			System.out.print("Substrate->");
			for (Iterator<Reaction> iterator = c.substrateOf.iterator(); iterator.hasNext();) 
			{
				Reaction r = (Reaction) iterator.next();
				System.out.print(r.id + " ");
			}
			
			System.out.print("\nProduced By->");
			for (Iterator<Reaction> iterator = c.producedBy.iterator(); iterator.hasNext();) 
			{
				Reaction r = (Reaction) iterator.next();
				System.out.print(r.id + " ");
			}
			System.out.println("\nTarget: "+c.isTarget());
			System.out.println("Precursor: "+c.isPrecursor());
			System.out.println("Bootstrap: "+c.isBootstrap());
			
			System.out.print("\n");
		}
		
		for (Iterator<Reaction> iter = reactions.values().iterator(); iter.hasNext();) 
		{
			Reaction r = (Reaction) iter.next();
			System.out.println(r.id);
		}
	}
	
	public void readFromSbmlFormat(String inputFile)
	{
		try
		{
			SBMLDocument xmlNetwork = (new SBMLReader()).readSBML(inputFile);
			Model network = xmlNetwork.getModel();
			
	        addCompounds(network);
	        addReactions(network);
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
		
	}

	public Compound addCompound(String id, String name, String compartment)
	{
		Compound c = new Compound(id, name, compartment);
		compounds.put(id, c);
		return c;
	}
	
	public void addCompounds(Model model)
	{
		int nc = model.getNumSpecies();
        for(int i = 0; i < nc; i++) {
            addCompound(model.getSpecies(i).getId(), model.getSpecies(i).getName(), model.getSpecies(i).getCompartment());
        }
	}

	public Reaction addNewReaction(String id, String name, boolean reversible)
	{
        Reaction r = new Reaction(id, name, reversible);
        reactions.put(id, r);
        return r;	
	}

	public void removeReactionDependencies(Reaction r)
	{
		for(Compound c: r.produces.values())
			c.producedBy.remove(r);
		for(Compound c: r.substrates.values())
			c.substrateOf.remove(r);
	}
	
	public void removeReaction(Reaction r)
	{
		removeReactionDependencies(r);
		if( r.isReversible() && r.getReverseReaction() != null ) {
			r.getReverseReaction().setReversible(false);
		}
		reactions.values().remove(r);
	}
	
	public void addReactions(Model network)
	{
		int nr = network.getNumReactions();
        for(int i = 0; i < nr; i++) {
            Reaction r1 = addNewReaction(network.getReaction(i).getId(), network.getReaction(i).getName(), network.getReaction(i).isReversible());
            Reaction r2 = null;
            if( r1.isReversible() ) {
            	r2 = addNewReaction(r1.getId()+"_REV", r1.getName()+"_REV", r1.isReversible());
            	r1.setReverse(r2);
            }
            
            // add the substrates of the reaction
            int ns = network.getReaction(i).getNumReactants();
            for(int j = 0; j < ns; j++) {
	            // Finds the compound
            	String cId = network.getReaction(i).getReactant(j).getSpecies();
            	Compound c = compounds.get( cId );
	            if( c != null)
	            {
	            	r1.addSubstrate(c);
	            	if( r1.isReversible() )
	            		r2.addProduct(c);
	            }
            }

            // add the products of the reaction
            int np = network.getReaction(i).getNumProducts();
            for(int j = 0; j < np; j++) {
	            // Finds the compound
            	String cId = network.getReaction(i).getProduct(j).getSpecies();            	
	            Compound c = compounds.get(cId);
	            if( c != null)
	            {
	            	r1.addProduct(c);
	            	if( r1.isReversible() )
	            		r2.addSubstrate(c);
	            }
            }        
        }
	}
	
	/*
	 * Returns true if the network can produce the compound c and false otherwise.
	 * 
	 */
	public boolean canProduce(Compound c)
	{
		for(Reaction r: reactions.values())
		{
			if( r.canSynthetize(c)  )
				return true;
		}
		return false;
	}
	
	
	/*
	 * Removes the compound c. If "removeReaction" is true, the reactions involving c are also removed. 
	 * 
	 */
	public void removeCompound(Compound c, boolean removeReaction)
	{
		if( removeReaction )
		{
			List<Reaction> reactionsInvolvingC = new ArrayList<Reaction>();
			for(Reaction r: reactions.values())
			{
				if( r.substrates.containsKey(c.getId()) || r.produces.containsKey(c.getId()) )
					reactionsInvolvingC.add(r);
			}
			for(int i = reactionsInvolvingC.size()-1; i >= 0; i--)
				removeReaction(reactionsInvolvingC.get(i));
			
		}
		compounds.values().remove(c);
	}
	
	/**
	 * Removes compounds from the network
	 * @param cpdsToRemove
	 */
	public void removeCompounds(List<Compound> cpdsToRemove) {

		HashMap<String, Compound> newListOfCompounds = new HashMap<String, Compound>();
		for(Compound cpd : compounds.values()) 
		{
			if(!cpdsToRemove.contains(cpd)) 
				newListOfCompounds.put(cpd.getId(), cpd);
		}
		compounds = newListOfCompounds;

		// update reactions removing the deleted compounds from their product/substrate lists.
		for(Reaction reaction : reactions.values()) {
			HashMap<String, Compound> newSubstrates = new HashMap<String, Compound>();
			for(Compound cpd : reaction.getSubstrates().values()) 
			{
				if(compounds.containsKey(cpd.getId())) {
					newSubstrates.put(cpd.getId(), cpd);
				}
			}
			reaction.substrates = newSubstrates;

			HashMap<String, Compound> newProducts = new HashMap<String, Compound>();
			for(Compound cpd : reaction.getProduces().values()) 
			{
				if(compounds.containsKey(cpd.getId())) {
					newProducts.put(cpd.getId(), cpd);
				}
			}
			reaction.produces = newProducts;
		}
		cleanEmptyReactions();
	}
	
	// Looks for compounds "c" marked as "non topological precursors" and transforms them into topological precursors
	// by adding a fake reaction "c_precursor -> c" and no reaction that produces "c_precursor".
	public void extendNonTopologicalPrecursors()
	{
		// Identify all the non topological precursors in the network
		List<Compound> nonTopologicalPrecursors = new ArrayList<Compound>();
		for(Compound c : getCompounds().values() )
		{
			if( c.isPrecursor() && !c.isTopologicalPrecursor() )
				nonTopologicalPrecursors.add(c);
		}
		
		// And adds to them the special reactions
		for(Compound c : nonTopologicalPrecursors )
		{
			// Create an special reaction that takes as substrate a special compound c_precursor  
			// and that produces the compound c.
			Compound precursor =addCompound("PRECURSOR_"+c.getId(), "PRECURSOR_"+c.getId(), "PRECURSOR_"+c.getId());
			precursor.setTopologicalPrecursor(true);
			Reaction special = addNewReaction("PRECURSOR_"+c.getId(), "PRECURSOR_"+c.getId(), false);
			special.addProduct(c);
			special.addSubstrate(precursor);
		}
	}	
	
	public void extendTopologicalPrecursors(boolean precursorIfProducedOnlyByReversible) 
	{
		for(Compound cpd : this.compounds.values()) 
		{
			List<Reaction> reactionsThatProduce = cpd.getReactionsThatProduce(false);
			if( reactionsThatProduce.size() == 0 )
			{
				cpd.setTopologicalPrecursor(true);
			}
			else if( precursorIfProducedOnlyByReversible && reactionsThatProduce.size() == 1 && cpd.getReactionsThatConsume(false).size() == 1 && reactionsThatProduce.get(0).isReversible() )
			{
				cpd.setTopologicalPrecursor(true);
			}
		}
	}
	
	public void removeTopologicalSourcesThatAreNotPrecursors() 
	{
		boolean changed = true;
		List<Compound> cpdsToRemove = new ArrayList<Compound>();
		while( changed ) {
			changed = false;
			cpdsToRemove.clear();
			for(Compound cpd : this.compounds.values()) 
			{
				List<Reaction> reactionsThatProduce = cpd.getReactionsThatProduce(true);
				if( reactionsThatProduce.size() == 0 && !cpd.isPrecursor() )
				{
					cpdsToRemove.add(cpd);
				}
			}
			
			if(cpdsToRemove.size() > 0) {
				changed = true;
				for(Compound c: cpdsToRemove) {
					//System.out.println("Removing "+c.getId()+" because it became a source.");
					removeCompound(c, true);
				}
			}
		}
	}
	
	public void removeEmptyCyclesForTarget(Compound target) {
		// simply does a "backward traversal" from the target. 
		// if a reaction creates an empty cycle it is removed
		clearAllCompoundsFlags();
		clearAllReactionFlags();

		HashMap<String, Reaction> reactionsToKeep = new HashMap<String, Reaction>();
		List<Compound> compoundsToFix = new ArrayList<Compound>();
		List<Compound> noSources = new ArrayList<Compound>();
		compoundsToFix.add(target);
		
		while( !compoundsToFix.isEmpty() ) {
			if( InputParameters.randomChoices ) {			
				Collections.shuffle(compoundsToFix);
			}
			Compound cpd = compoundsToFix.get(0);
			
			// for each reaction producing the compound
			List<Reaction> reactionsThatProduce = new ArrayList<Reaction>();
			for(Reaction r: cpd.getProducedBy()) {
				if( !r.isFlag() ) {
					reactionsThatProduce.add(r);					
				}
			}
			
			// if there is no new reaction that produces this compound, then it can be removed from
			// the compoundsToFix list
			if( reactionsThatProduce.size() == 0 ) {
				compoundsToFix.remove(0);
				continue;
			}
			// if there are reactions to analyze
			
			// set the compound as analyzed
			if( !cpd.isFlag() ) {
				cpd.setFlag();
			}

			// randomize list of reactions
			if( InputParameters.randomChoices ) {
				Collections.shuffle(reactionsThatProduce);
			}

			// takes one of the possible reactions
			Reaction r = reactionsThatProduce.get(0);
			// set the reaction as analyzed
			if( !r.isFlag() ) {
				r.setFlag();
			}			

			reactionsToKeep.put(r.getId(), r);
			// let us validate if r creates an empty cycle
			MaximalTarget mt = new MaximalTarget(this);
			mt.run(reactionsToKeep, noSources);
			// if the maximal target of the empty set is not empty, removes the reaction
			if( !mt.isEmpty() ) {
				reactionsToKeep.remove(r.getId());
				continue;
			}
					
			// now, enqueue each substrate for further analyze
			for(Compound c: r.getSubstrates().values() ) {
				if( !c.flag ) {
					compoundsToFix.add(c);
				}
			}
		}
		
		List<Reaction> reactionsToRemove = new ArrayList<Reaction>();		
		// remove all reactions not reached by the backward - bfs search
		for(Reaction r: getReactions().values()) {
			if( !reactionsToKeep.containsKey(r.getId()) ) {
				reactionsToRemove.add(r);
			}
		}
		
		for(Reaction rToDel: reactionsToRemove) {
			//System.out.println("Cutting "+rToDel.id);
			removeReaction(rToDel);
		}		
	}	
	
	/*
	 * This procedure eliminates reactions which has product or substrates side empty
	 */
	public void cleanEmptyReactions() {
		List<Reaction> reactionsToRemove = new ArrayList<Reaction>();
		
		// for each reaction checks whether their products or substrates are empty
		for(Reaction r: getReactions().values()) {
			if( r.getProduces().isEmpty() || r.getSubstrates().isEmpty() ) {
				reactionsToRemove.add(r);
			}
		}
		
		for(Reaction rToDel: reactionsToRemove) {
			removeReaction(rToDel);
		}		
	}	
	
	/*
	 * This procedure eliminates repeated reactions 
	 */
	public void cleanRepeatedReactions() {
		List<Reaction> reactionsToRemove = new ArrayList<Reaction>();
		
		List<Reaction> reactions = new ArrayList<Reaction>(getReactions().values());

		// check repetitions and add them to be deleted
		for(int i = 0; i < reactions.size(); i++) {
			
			// if the i-th reaction is already marked for removal, ignore it.
			if( reactionsToRemove.contains(reactions.get(i)) ) {
				continue;
			}
			
			// search repeated reactions and mark them
			for(int j = i+1; j < reactions.size(); j++) {
				if( reactions.get(i).equals(reactions.get(j)) ) {
					reactionsToRemove.add(reactions.get(j));
				}
			}
		}
		
		// delete the repeated reactions
		for(Reaction rToDel: reactionsToRemove) {
			System.out.println("Removing repeated reaction "+rToDel);
			removeReaction(rToDel);
		}		
	}	
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public void cleanOrphanCompounds() {
		List<Compound> compoundsToRemove = new ArrayList<Compound>();
		
		// for each compound, check if it is not involved in any reaction
		for(Compound c: getCompounds().values()) {
			if( c.getProducedBy().isEmpty() && c.getSubstrateOf().isEmpty() ) {
				compoundsToRemove.add(c);
			}
		}
		
		for(Compound cToDel: compoundsToRemove) {
			removeCompound(cToDel, false);
		}
	}
	
	/*
	 * Initializes side compounds set with the product set for all reactions
	 * 
	 */
	public void initSideCompounds() {
		for(Reaction r: getReactions().values()) {
			r.initSideCompounds();
		}
	}
	
	public void removeCompoundsFromReactionsIfPresentAsSubstrateAndProduct() {
		for(Reaction r: getReactions().values()) {
			List<Compound> catalystCompounds = new ArrayList<Compound>();
			for(Compound s: r.getSubstrates().values()) {
				for(Compound p: r.getProduces().values() ) {
					if( p.getId().equals(s.getId()) ) {
						catalystCompounds.add(s);
						break;
					}
				}
			}
			
			// for each catalyst compound, remove it from both sides of the reaction
			for(Compound c: catalystCompounds) {
				r.removeSubstrate(c);
				r.removeProduct(c);
			}
		}
	}

	public String toString() {
	
		String str = new String();
		
		for(Reaction reaction : reactions.values()) {
			str = str.concat(reaction+"\n");
		}
		
		return str;
	}

	public HashMap<String, Compound> getCompounds() {
		return compounds;
	}

	public void setCompounds(HashMap<String, Compound> compounds) {
		this.compounds = compounds;
	}
	
	public int getNumPrecursors() {
		int numPrecursors = 0;
		for(Compound c: getCompounds().values()) {
			if( c.isPrecursor() )
				numPrecursors++;
		}
		return numPrecursors;
	}

	public HashMap<String, Reaction> getReactions() {
		return reactions;
	}

	public void setReactions(HashMap<String, Reaction> reactions) {
		this.reactions = reactions;
	}
	
	public void clearAllCompoundsFlags() {
		for(Compound c: compounds.values()) {
			c.clearFlag();
		}
	}
	
	public void clearAllReactionFlags() {
		for(Reaction r: reactions.values()) {
			r.clearFlag();
		}
	}	
	
}