/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;
import metabolicNetwork.Reaction;

public class MaximalTarget {

	MetabolicNetwork network;
	HashSet<Compound> availableCompounds = new HashSet<Compound>();
	
	public MaximalTarget(MetabolicNetwork network) {
		this.network = network;
	}
	
	public void run(HashMap<String, Reaction> availableReactions, List<Compound> sources)
	{				
		boolean changed = true;

		List<Reaction> reactionsToUse = new ArrayList<Reaction>();
		reactionsToUse.addAll(availableReactions.values());
		
		computeAvailableCompounds(reactionsToUse, sources);
		
		// repeat while there is no fixed point
		while(changed)
		{
			changed = false;
			
			int l = reactionsToUse.size();
			for (int i = l-1; i>=0; i--) 
			{
				Reaction reaction = reactionsToUse.get(i);
				
				// Checks whether the reaction is ready, i.e, if has no dependency
				List<Compound> synthetized = new ArrayList<Compound>();
				synthetized.addAll(availableCompounds);
				if( !reaction.ready(sources, sources, synthetized))
				{
					reactionsToUse.remove(i);
					changed = true;
				}
			}
			computeAvailableCompounds(reactionsToUse, sources);
		}
	}
	
	public boolean contains(Compound c) {
		return availableCompounds.contains(c);
	}
	
	public boolean isEmpty() {
		return availableCompounds.isEmpty();
	}

	protected void computeAvailableCompounds(List<Reaction> availableReactions, List<Compound> sources) {
		availableCompounds.clear();
		availableCompounds.addAll(sources);
		for(Reaction r: availableReactions) {
			availableCompounds.addAll(r.getProduces().values());
		}
	}
	
}
