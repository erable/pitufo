package application;

import java.util.ArrayList;
import java.util.List;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;

public class MaximalTargetValidation {

	MetabolicNetwork network;
	
	public MaximalTargetValidation(MetabolicNetwork network) {
		this.network = network;
	}

	public boolean isPrecursor(Compound target, PrecursorSet s) {
		List<Compound> sources = new ArrayList<Compound>();
		for(Compound c: s.precursors) {
			sources.add(c);
		}
		MaximalTarget mt = new MaximalTarget(network);
		mt.run(network.getReactions(), sources);
		return mt.contains(target);
	}

}
