/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

import java.util.ArrayList;
import java.util.List;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;
import metabolicNetwork.Reaction;

public class ForwardPropagation {

	MetabolicNetwork network;
	List<Compound> synthetized = new ArrayList<Compound>();
	List<Reaction> availableReactions = new ArrayList<Reaction>();
	
	public ForwardPropagation(MetabolicNetwork network) {
		this.network = network;
	}
	
	public List<Compound> run(List<Compound> inputList, List<Compound> bootstrapList)
	{
		boolean newReactionFired = true;
		synthetized.clear();
		
		// initially, all reactions are available
		availableReactions.clear();
		availableReactions.addAll(network.getReactions().values());
		
		// repeat while new reactions are fired
		while(newReactionFired)
		{
			newReactionFired = false;
			
			int l = availableReactions.size();
			for (int i = l-1; i>=0; i--) 
			{
				Reaction reaction = availableReactions.get(i);
				// Checks whether the reaction is ready, i.e, if has no dependency
				if( reaction.ready(inputList, bootstrapList, synthetized) )
				{
					// If so, the reaction then fires and all of their productions are added to the synthetized set
					for(Compound c: reaction.getProduces().values()) {
						if( !synthetized.contains(c) )
							synthetized.add(c);
						newReactionFired = true;
					}
					availableReactions.remove(i);
				}
			}
		}
		return synthetized;
	}
	
	public boolean fired(Reaction r) {
		if( availableReactions.contains(r) && 
				(!r.isReversible() || availableReactions.contains(r.getReverseReaction())))
			return false;
		return true;
	} 
	
	public boolean produced(Compound c) {
		return synthetized.contains(c);
	}
	
}
