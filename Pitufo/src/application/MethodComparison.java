package application;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;

public class MethodComparison {

	private File outputFile = null;
	private BufferedWriter writer = null;
	private String experimentName;
	private int iteration;
	private String algorithm;
	
	private Map<String, PrecursorSets> solutions = new HashMap<String, PrecursorSets>();
	private Map<String, Integer> iterationsWithNoNewSolution = new HashMap<String, Integer>();

	private Map<String, Integer> recursions = new HashMap<String, Integer>();
	private Map<String, Long> timeInMs = new HashMap<String, Long>();

	
	public MethodComparison(String experimentName) {
		this.experimentName = experimentName;
	}

	public void startExperiment(String algorithm) {
		this.algorithm = algorithm;
		iteration = 1;
	}
	
	public void finish() {
		// once finished the time comparison step, let us compute the final minimal solutions
		// outputs the result for each target
		System.out.println("\n\n%%%%%%%%% - now consolidating solutions for each target...\n\n");
		for(String t: getSolutions().keySet()) {
			System.out.println("Target: "+t);
			
			try {
				BufferedWriter outputFile = new BufferedWriter(new FileWriter(experimentName+"-target-"+t+".csv"));
				
				PrecursorSets solutions = getSolutions().get(t);
				List<PrecursorSet> minSols = solutions.minimalize();
				
				System.out.println("# solutions found: "+minSols.size());
				for(PrecursorSet sol: minSols) {
					System.out.println(sol);
					outputFile.write(sol+"\n");
				}
				System.out.println("----");
				outputFile.close();
			}
			catch(Exception e) {
				System.out.println("Error trying to close file."+e.getMessage());				
			}
				
		}		
		
		// and append to the output file a summary
		if( writer != null ) {
			
			try {
				
				for(String target: solutions.keySet()) {
				
					writer.write(algorithm + ";" + 
								 experimentName + ";" +
								 "summary after "+iteration+ " iterations;" +
								 ";" +
								 ";" +
								 target + ";" +						 
								 target + ";" +
								 ";" +
								 ";" +
								 ";" +
								 getSolutions().get(target).solutions.size() + ";" +
								 (getRecursions().get(target) / iteration) + ";" +
								 getTimeInMs().get(target)+ ";" +
								 printSolutions(getSolutions().get(target).solutions) + "\n"						 
							);
				}
			}
			catch(Exception e) {
				System.out.println("Error trying to write to the output file.");
			}			
			
			try {
				writer.close();
			}
			catch(Exception e) {
				System.out.println("Error trying to close file."+e.getMessage());
			}
		}
		
	}
	
	private void writeHeader() {
		try {
			writer.write("algorithm;" +
					"network;" +
					"iteration;" +
					"#comp;" +
					"#reac;" +
					"target_id;" +
					"target_name;" +
					"#ppn.comp;" +
					"#ppn.reac;" +
					"#ppn.sources;" +
					"#sol;" +
					"#rec;" +
					"time;" +
					"numNewSolutions;" +
					"solutions\n");
		}
		catch(Exception e) {
			System.out.println("Error trying to write to the output file.");
		}		
	}
	
	public void register(Compound target, MetabolicNetwork originalNetwork, MetabolicNetwork preprocessedNetwork,
			List<PrecursorSet> solutions, int numRecursions, long timeInMs) {
		
		int numNew = 0;
		
		// save the result for this target
		PrecursorSets sols = getSolutions().get(target.getId());
		if( sols != null ) {
			sols.setChanged(false);
			numNew = sols.addSolutions(solutions);
			// counting the number of iterations passed without changing the solution set
			if( sols.isChanged() ) {
				getIterationsWithNoNewSolution().put(target.getId(), 0);
			} else {
				Integer n = getIterationsWithNoNewSolution().get(target.getId());
				getIterationsWithNoNewSolution().put(target.getId(), n+1);
			}
			
			// accumulating number of recursions
			Integer nRec = getRecursions().get(target.getId());
			getRecursions().put(target.getId(), nRec + numRecursions);
			
			// accumulating amount of time spent on computing solutions
			Long time = getTimeInMs().get(target.getId());
			getTimeInMs().put(target.getId(), time + timeInMs);
		}
		else {
			sols = new PrecursorSets();
			numNew = sols.addSolutions(solutions);
			getSolutions().put(target.getId(), sols);
			getIterationsWithNoNewSolution().put(target.getId(), 0);
			getTimeInMs().put(target.getId(), timeInMs);
			getRecursions().put(target.getId(), numRecursions);
		}
		
		
		if( writer == null ) {
			prepareFile();
		}
		
		try {
			writer.write(algorithm + ";" + 
						 experimentName + ";" +
						 iteration + ";" +
						 originalNetwork.getCompounds().size() + ";" +
						 originalNetwork.getReactions().size() + ";" +
						 target.getId() + ";" +						 
						 target.getName() + ";" +
						 preprocessedNetwork.getCompounds().size() + ";" +
						 preprocessedNetwork.getReactions().size() + ";" +
						 preprocessedNetwork.getNumPrecursors()+";" +
						 (solutions == null ? 0 : solutions.size()) + ";" +
						 numRecursions + ";" +
						 timeInMs + ";" +
						 numNew + ";" +
						 (solutions == null ? "expired" : printSolutions(solutions)) + "\n"						 
					);
		}
		catch(Exception e) {
			System.out.println("Error trying to write to the output file.");
		}
		
	}
	
	public void nextIteration() {
		iteration++;
	}

	private void prepareFile() {
		outputFile = new File(experimentName+".csv");
		try {
			writer = new BufferedWriter(new FileWriter(outputFile));
			writeHeader();
		}
		catch(Exception e) {
			System.out.println("Error trying to create output file "+experimentName+".csv");
		}
	}
	
	private String printSolutions(Collection<PrecursorSet> solutions) {
		StringBuilder result = new StringBuilder();
		int i = 1;
		for(PrecursorSet set: solutions) {
			result.append("#"+ i++ + " {" + printSolution(set) +" } "); 
		}
		return result.toString();
	}
	
	private String printSolution(PrecursorSet solution) {
		StringBuilder result = new StringBuilder();
		for(int i = 0; i < solution.precursors.size(); i++) {
			if( i > 0 ) {
				result.append(", ");
			}
			if( solution.precursors.get(i).isPrecursor() ) {
				result.append( solution.precursors.get(i).getId());
			}
		}
		return result.toString();
	}

	public Map<String, PrecursorSets> getSolutions() {
		return solutions;
	}

	public void setSolutions(Map<String, PrecursorSets> solutions) {
		this.solutions = solutions;
	}
	
	public boolean stop() {
		// check if all the targets being computed have "converged"
		for(Integer n: getIterationsWithNoNewSolution().values()) {
			if( n < InputParameters.stopNoNew )	// if the number of iterations of one target is not yet at the threshold, then it should not stop
				return false;
		}
		
		System.out.println("-=-=-=- FINISHING BECAUSE AFTER "+InputParameters.stopNoNew+" iterations NO NEW SOLUTION WAS FOUND FOR NONE OF THE TARGETS");
		return true;
	}

	public Map<String, Integer> getIterationsWithNoNewSolution() {
		return iterationsWithNoNewSolution;
	}

	public void setIterationsWithNoNewSolution(
			Map<String, Integer> iterationsWithNoNewSolution) {
		this.iterationsWithNoNewSolution = iterationsWithNoNewSolution;
	}

	public Map<String, Integer> getRecursions() {
		return recursions;
	}

	public void setRecursions(Map<String, Integer> recursions) {
		this.recursions = recursions;
	}

	public Map<String, Long> getTimeInMs() {
		return timeInMs;
	}

	public void setTimeInMs(Map<String, Long> timeInMs) {
		this.timeInMs = timeInMs;
	}
	
}
