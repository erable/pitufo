package application;

import java.util.HashSet;
import java.util.Set;

public class ComparePrecursorSetsSolution {
	Set<String> solution = new HashSet<String>();
	int quantity;
	
	public String toString() {
		return solution.toString();
	}
	
	public boolean equals(ComparePrecursorSetsSolution other) {
		return solution.equals(other.solution);
	}
}
