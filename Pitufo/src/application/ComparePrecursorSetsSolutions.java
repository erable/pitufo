package application;

import java.util.HashSet;
import java.util.Set;

public class ComparePrecursorSetsSolutions {
	Set<ComparePrecursorSetsSolution> solutions = new HashSet<ComparePrecursorSetsSolution>();
		
	public void registerSolution(ComparePrecursorSetsSolution s) {
		
		for(ComparePrecursorSetsSolution sol: solutions) {
			if( sol.equals(s) ) {
				sol.quantity++;
				return;
			}
		}
		s.quantity = 1;
		solutions.add(s);
	}
}
