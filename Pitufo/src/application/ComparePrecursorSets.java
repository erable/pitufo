package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComparePrecursorSets {

	Map<String, ComparePrecursorSetsSolutions> solutions = new HashMap<String, ComparePrecursorSetsSolutions>();
	
	public static void main(String[] args) throws IOException {
		// First, let us check if the user passed the name of the file to analyze
		if( args.length < 1 ) {
			System.out.println("Put the name of the file to analyze as the 1st argument to the program.");
			System.exit(0);
		}
		// then we extract the solutions
		ComparePrecursorSets cps = extractPrecursorSetsFromFile(args[0]);

		// outputs the result
		for(String target: cps.solutions.keySet()) {
			System.out.println("Target: "+target);
			
			BufferedWriter outputFile = new BufferedWriter(new FileWriter("target-"+target+".csv"));
			
			ComparePrecursorSetsSolutions solutions = cps.solutions.get(target);
			System.out.println("# solutions found: "+solutions.solutions.size());
			for(ComparePrecursorSetsSolution sol: solutions.solutions) {
				System.out.println(sol + ": " +sol.quantity + " times");
				outputFile.write(sol+";"+sol.quantity+"\n");
			}
			System.out.println("----");
			outputFile.close();
		}
	}
	
	protected static ComparePrecursorSets extractPrecursorSetsFromFile(String filename) throws IOException {
		/*
		algorithm;network;iteration;#comp;#reac;target_id;target_name;#ppn.comp;#ppn.reac;#ppn.sources;#sol;#rec;time;solutions
		Pitufina (Min);teste;1;5;4;T;T;7;4;4;1;4;15;#1 {A } 
		Pitufina (Min);teste;2;5;4;T;T;7;4;4;1;4;4;#1 {A } 
		Pitufina (Min);teste;3;5;4;T;T;7;4;4;1;4;11;#1 {A } 
		Pitufina (Min);teste;4;5;4;T;T;7;4;4;1;4;4;#1 {B } 
		*/		
		ComparePrecursorSets cps = new ComparePrecursorSets();
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String line = reader.readLine(); // the header
		if( line != null ) {
			line = reader.readLine();	// reads the first line of data
		}
		while( line != null ) {
			String[] contents = line.split(";");
						
			if( contents.length == 14 ) {
				String target = contents[5];
				ComparePrecursorSetsSolutions solutionsForTarget = cps.solutions.get(target);
				if( solutionsForTarget == null ) {
					solutionsForTarget = new ComparePrecursorSetsSolutions();
					cps.solutions.put(target, solutionsForTarget);
				}

				String solutions = contents[13];
				for(ComparePrecursorSetsSolution solution: splitSolutions(solutions)) {
					solutionsForTarget.registerSolution(solution);
				}
			}
			line = reader.readLine();
		}
		reader.close();
		return cps;
	}
	
	protected static List<ComparePrecursorSetsSolution> splitSolutions(String solutions) {
		List<ComparePrecursorSetsSolution> sols = new ArrayList<ComparePrecursorSetsSolution>();
		
		while(!solutions.trim().isEmpty()) {
			String sol = solutions.substring(solutions.indexOf('{')+1, solutions.indexOf('}'));
			
			ComparePrecursorSetsSolution s = new ComparePrecursorSetsSolution();
			for(String compound: sol.split(",") ) {
				s.solution.add(compound.trim());
			}
			sols.add(s);
			
			solutions = solutions.substring(solutions.indexOf('}')+1, solutions.length());
		}
		
		return sols;
	}

}
