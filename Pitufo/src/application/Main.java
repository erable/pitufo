/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import metabolicNetwork.Compound;
import metabolicNetwork.MetabolicNetwork;
import metabolicNetwork.Reaction;
import pitufo.Pitufo;
import pitufo.PrecursorFinder;
import pitufolandia.Gargamel;
import pitufolandia.PapaPitufo;
import pitufolandia.PapaPitufo2;
import pitufolandia.Pitufina;
import utils.ArgumentParser;
import utils.StringUtils;

public class Main {

	MetabolicNetwork network;	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		ArgumentParser p = new ArgumentParser(args);
        if (args.length == 0 || p.hasOption("h")) 
        	printUsage();
        else 
        {
        	if(! p.hasOption("s") || !p.hasOption("i")) 
        		printUsage();
        	System.out.println("Started at "+new Date());

        	InputParameters.printForward = p.hasOption("pF") || p.hasOption("v");
        	InputParameters.printNetwork = p.hasOption("pN") || p.hasOption("v");
        	InputParameters.oneByOne = p.hasOption("o");
        	InputParameters.keepForwardResultInTheTree = p.hasOption("k");
        	InputParameters.minimalityCheck = p.hasOption("cm");
        	InputParameters.forwardPropagation = p.hasOption("fp");
        	InputParameters.reductionThroughMaximalTarget = p.hasOption("rMT");
        	InputParameters.precursorIfProducedOnlyByReversible = !p.hasOption("pr") || !p.getOption("pr").equals("off");
        	InputParameters.emptyCyclesEnumeration = p.hasOption("ece");
        	InputParameters.emptyCyclesCut = p.hasOption("ecc");
        	InputParameters.randomChoices = !p.hasOption("random") || !p.getOption("random").equalsIgnoreCase("OFF");
        	InputParameters.stopNoNew = p.hasOption("stop-no-new") ? Integer.parseInt(p.getOption("stop-no-new")) : 0;
        	
			String sbmlFile = p.getOption("s");
			String inputFile = p.getOption("i");
			
			String experimentName = p.getOption("e");
			boolean experimentMode = experimentName != null;
			int iterations = 1;
			MethodComparison mc = null;
			if( experimentMode ) {
				mc = new MethodComparison(experimentName);
				iterations = 3;
			}
			
			if( p.hasOption("ni") ) {	// number of iterations (user defined)
				iterations = Integer.parseInt(p.getOption("ni"));
			}
			
			printGPLShortNotice();
							
			System.out.println("SBML File:"+sbmlFile);
			System.out.println("Input File:"+inputFile);
			
			Main main = new Main(sbmlFile, inputFile);
			
			// Extend topological precursors (mark as precursor), bootstraps, do the forward propagation step, the maximal target reduction
			main.preprocessNetwork();
			
			// Eliminate from "targets" the targets that were produced during forward propagation
			for(int j=InputParameters.getTargetCompounds().size()-1; j >= 0; j--)
			{
				if(! main.getNetwork().getCompounds().values().contains( InputParameters.getTargetCompounds().get(j) ) )
					InputParameters.getTargetCompounds().remove(j);
			}
			System.out.println("--------------------------------------------------");
			System.out.println("Target compounds not produced during the forward propagation : ");
			for(Compound cpd : InputParameters.getTargetCompounds()) {
				System.out.println(StringUtils.sbmlDecode(cpd.getId()));
			}

			PrecursorFinder finder;
			// Finding precursors with the original method (replacement tree)
			if( p.hasOption("mT") )
			{
				if( experimentMode ) {
					mc.startExperiment("Pitufo");
				}
				finder = new Pitufo(main.getNetwork(), p.hasOption("pEmpty"));
				for(int i = 0; i < iterations; i++) {
					((Pitufo)finder).findPrecursorWithReplacementTree(false, mc);
					if( experimentMode ) {
						if( mc.stop() ) {
							break;
						}						
						mc.nextIteration();
					}
				}
			}
			
			if( p.hasOption("mP") )
			{
				if( experimentMode ) {
					mc.startExperiment("Pitufina");
					InputParameters.minimalityCheck = false;
				}
				finder = new Pitufina(main.getNetwork(), p.hasOption("pEmpty"));
				for(int i = 0; i < iterations; i++) {
					((Pitufina)finder).findPrecursorsInNetwork(InputParameters.getTargetCompounds(), mc);
					if( experimentMode ) {
						if( mc.stop() ) {
							break;
						}						
						mc.nextIteration();
					}
				}
			}
			
			if( p.hasOption("mPMin") )
			{
				if( experimentMode ) {
					mc.startExperiment("Pitufina (Min)");
					InputParameters.minimalityCheck = true;
				}
				finder = new Pitufina(main.getNetwork(), p.hasOption("pEmpty"));
				for(int i = 0; i < iterations; i++) {
					((Pitufina)finder).findPrecursorsInNetwork(InputParameters.getTargetCompounds(), mc);
					if( experimentMode ) {
						if( mc.stop() ) {
							break;
						}						
						mc.nextIteration();
					}
				}
			}

			if( p.hasOption("mPP") )
			{
				if( experimentMode ) {
					mc.startExperiment("Papa Pitufo");
					InputParameters.minimalityCheck = false;
				}
				finder = new PapaPitufo(main.getNetwork(), p.hasOption("pEmpty"));
				for(int i = 0; i < iterations; i++) {
					((PapaPitufo)finder).findPrecursorsInNetwork(InputParameters.getTargetCompounds(), mc);
					if( experimentMode ) {
						if( mc.stop() ) {
							break;
						}
						mc.nextIteration();
					}
				}
			}
			
			if( p.hasOption("mPPMin") )
			{
				if( experimentMode ) {
					mc.startExperiment("Papa Pitufo (Min)");
					InputParameters.minimalityCheck = true;
				}
				finder = new PapaPitufo(main.getNetwork(), p.hasOption("pEmpty"));
				for(int i = 0; i < iterations; i++) {
					((PapaPitufo)finder).findPrecursorsInNetwork(InputParameters.getTargetCompounds(), mc);
					if( experimentMode ) {
						if( mc.stop() ) {
							break;
						}					
						mc.nextIteration();
					}
				}
			}
			
			if( p.hasOption("mPP2") )
			{
				if( experimentMode ) {
					mc.startExperiment("Papa Pitufo Paper 2");
					InputParameters.minimalityCheck = false;
				}
				finder = new PapaPitufo2(main.getNetwork(), p.hasOption("pEmpty"));
				for(int i = 0; i < iterations; i++) {
					((PapaPitufo2)finder).findPrecursorsInNetwork(InputParameters.getTargetCompounds(), mc);
					if( experimentMode ) {
						if( mc.stop() ) {
							break;
						}					
						mc.nextIteration();
					}
				}
			}		
			
			if( p.hasOption("mG") )
			{
				if( experimentMode ) {
					mc.startExperiment("Gargamel");
					InputParameters.minimalityCheck = false;
				}
				finder = new Gargamel(main.getNetwork(), false);
				for(int i = 0; i < iterations; i++) {
					((Gargamel)finder).findPrecursorsInNetwork(InputParameters.getTargetCompounds(), mc);
					if( experimentMode ) {
						if( mc.stop() ) {
							break;
						}						
						mc.nextIteration();
					}
				}
			}
			
			if( experimentMode ) {
				mc.finish();
			}
			
			System.out.println("Finished at "+new Date());
        }
	}
	
	public static void printGPLShortNotice()
	{
	    System.out.println("--------------------------------------------");
		System.out.println("PITUFO  Copyright (C) 2010  Cottret & Milreu");
	    System.out.println("This program comes with ABSOLUTELY NO WARRANTY.");
	    System.out.println("This is free software, and you are welcome to redistribute it");
	    System.out.println("under certain conditions; check the COPYING file on the sources directory for details.");
	    System.out.println("--------------------------------------------");
	}	
	
	public static void printUsage() {
		
		System.err.println("--------------PITUFO-----------------------");
		System.err.println("-s=filename\tSBML file");
		System.err.println("-i=filename\tInput file");
		System.err.println("-d\t\tDecode SBML identifiers");
		System.err.println("-v\t\tequivalent ot -pF -pN -PT");
		System.err.println("-o\t\tLook separately at each target compound");
		System.err.println("-pr=off\t\tTurns off the treatment of compounds that are only produced by a reversible reaction as potential precursors.");
		System.err.println("-pEmpty\t\tConsider the solution <empty set> as a special solution");
		System.err.println("-rMT\t\tReduces the network by applying the maximal target to the user-defined precursors");		
		System.err.println("-fp\t\tPerforms a forward propagation for reducing the size of the input network");
		System.err.println("-cm\t\tPerforms the minimality check among reactions (pruning)");
		System.err.println("-ece\t\tEnumerate empty cycles (preprocessing step)");
		System.err.println("-ecc\t\tEmpty cycles cut (preprocessing step)");
		System.err.println("-random=off\t\tTurns off the random choices of substrates and reactions in the preprocessing steps.");
		System.err.println("-ni=NUMBER\t\tDefines the number of iterations to run (default=3).");		
		System.err.println("-stop-no-new=NUMBER\t\tDefines a stop condition if there is no new solution found after NUMBER iterations. (default=0)");		
		System.err.println("-mT\t\tUses the PITUFO method building a replacement tree");
		System.err.println("-mP\t\tUses the PITUFINA method (in network, without merge, without tree)");
		System.err.println("-mPP\t\tUses the PAPA PITUFO method finding the precursors directly in the network (merge)");
		System.err.println("-mPMin\t\tUses PITUFINA but with minimal condition for chosing reactions.");
		System.err.println("-mPPMin\t\tUses PATA PITUFO but with minimal condition for chosing reactions.");
		System.err.println("Debugging options :");
		System.err.println("-pF\t\tPrint Forward Results");
		System.err.println("-pN\t\tPrint Network");
		System.err.println("-pT\t\tPrint Replacement Tree");
		System.err.println("--------------------------------------------");
		
		System.exit(0);
	}
	
	public Main(MetabolicNetwork network, Set<String> inputIds, Set<String> bootstrapIds, Set<String> targetIds, Set<String> precursorIds) {
		
		this.setNetwork(network);
		
		for(String id : inputIds) {
			Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null)
        		System.out.println("Input Compound won't find. Id = " + id);
        	else
        	{
        		InputParameters.getInputCompounds().add(c);
        	}
		}
		
		for(String id : bootstrapIds) {
			Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null)
        		System.out.println("Bootstrap Compound won't find. Id = " + id);
        	else
        	{
        		InputParameters.getBootstrapCompounds().add(c);
        	}
		}
		
		for(String id : targetIds) {
			Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null) {
        		System.out.println("Target Compound won't find. Id = " + id);
        		// Creation
        		c = new Compound(id, id, "");
        		this.getNetwork().getCompounds().put(id, c);
        	}
        	else
        	{
        		InputParameters.getTargetCompounds().add(c);
        	}
		}
		
		for(String id : precursorIds) {
			Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null)
        		System.out.println("Precursor Compound won't find. Id = " + id);
        	else
        		c.setUserDefinedPrecursor(true);
		}
	}
	

	public Main(String sbmlFile, String inputFile) 
	{
		MetabolicNetwork network = new MetabolicNetwork();		
		network.readFromSbmlFormat(sbmlFile);
		System.out.println("\nThe original network contains "+network.getReactions().size()+" reactions and "+network.getCompounds().size()+" compounds.");
		
		this.setNetwork(network);
		this.readInputFile(inputFile);
	}
	
	public MetabolicNetwork getNetwork() {
		return network;
	}

	public void preprocessNetwork()
	{
		// mark compounds defined as bootstraps
		for(Compound cpd : InputParameters.getBootstrapCompounds()) {
			cpd.setBootstrap(true);
		}
		
		// Removing the forbidden precursors from the network
		removeForbiddenPrecursors();
		
		if( InputParameters.forwardPropagation ) {
			// Do the forwarding propagation of the network...
			List<Compound> notProduced = new ArrayList<Compound>();
			ForwardPropagation fp = new ForwardPropagation(network);
			List<Compound> compoundsSynthetized = fp.run(InputParameters.getInputCompounds(), InputParameters.getBootstrapCompounds());
			
			// Outputs the results...
			List<Compound> forwardResult = new ArrayList<Compound>(compoundsSynthetized);
			forwardResult.addAll(InputParameters.getInputCompounds());
			forwardResult.addAll(InputParameters.getBootstrapCompounds());
			System.out.println("\nThe original network contains "+network.getReactions().size()+" reactions and "+network.getCompounds().size()+" compounds.");
			
			for(Compound cpd : InputParameters.getTargetCompounds()) 
			{
				if(! forwardResult.contains(cpd))
					notProduced.add(cpd);
			}
			for(Compound cpd : forwardResult) {
				cpd.setBootstrap(true);
			}
			if( ! InputParameters.keepForwardResultInTheTree )
			    network.removeCompounds(forwardResult);
			if( InputParameters.printForward ) 
			{
				System.out.println("\nForward propagation results: "+forwardResult.size() + " compounds.");
				System.out.println("--- FORWARD PROPAGATION: Compounds synthetized. ");
				if( forwardResult.size() == 0 )
					System.out.println("None");
				else
				{
					for (Compound compound : forwardResult) 
						System.out.println(compound.getId());
				}
				System.out.println();
			}
			System.out.println("Forward propagation results: "+forwardResult.size()+" compounds.");
			System.out.println("Forward propagation results: "+forwardResult);
			System.out.println("\nThe reduced network contains "+network.getReactions().size()+" reactions and "+network.getCompounds().size()+" compounds.");
		}
		
		// 1st phase - choose direction for reversible reactions
		if( InputParameters.emptyCyclesEnumeration ) {
			if( InputParameters.getTargetCompounds().isEmpty() ) {
				System.out.println("No target defined. Keeping reversible reactions intact.");
			}
			else {
				Compound t = InputParameters.getTargetCompounds().get(0);
				System.out.println("Fixing direction of reversible reactions from the target "+t.getId());				
				network.removeEmptyCyclesForTarget(t);
			}
		}
		
		// The idea of this reduction is to eliminate every compound which is not a user-defined precursor
		// or that cannot be produced from them.
		if( InputParameters.reductionThroughMaximalTarget )
		{
			System.out.println("\nInitializing maximal target reduction");			
			boolean maximalTarget = false;
			while( !maximalTarget )
			{
				maximalTarget = true;
				
				// 1st. step - detect all compounds that cannot be produced from the whole network
				// and remove them and the reactions they're involved
				Compound[] listCompounds = (Compound[]) network.getCompounds().values().toArray(new Compound[0]);
				for(int i = listCompounds.length-1; i >= 0; i--)
				{
					Compound c = listCompounds[i];
					if( !c.isPrecursor() && !network.canProduce(c) )
					{
						System.out.println("Removing "+c+". Not in maximal target.");
						network.removeCompound(c, true);
						maximalTarget = false;
					}
				}

				// 2nd step - detect all compounds that are left with no reactions producing or consuming them
				listCompounds = (Compound[]) network.getCompounds().values().toArray(new Compound[0]);
				for(int i = listCompounds.length-1; i >= 0; i--)
				{
					Compound c = listCompounds[i];
					if( c.getReactionsThatProduce(false).size() == 0 && c.getReactionsThatConsume(false).size() == 0 )
					{
						System.out.println("Removing "+c+". Orphan compound (after compression).");
						network.removeCompound(c, false);
						maximalTarget = false;
					}
				}
			}
			System.out.println("\nThe reduced network after maximal target compression has "+network.getReactions().size()+" reactions and "+network.getCompounds().size()+" compounds.");			
		}

		// 2nd phase - network modification to include compounds for reactions and the artificial target
		if( InputParameters.emptyCyclesEnumeration ) {
			// take a list of the reactions (before any modification)
			List<Reaction> reactions = new ArrayList<Reaction>();
			reactions.addAll(network.getReactions().values());
			
			// creates a special target
			Compound target = network.addCompound("_T_", "_T_", "The Target");
			InputParameters.getTargetCompounds().clear();
			InputParameters.getTargetCompounds().add(target);			
			// and creates a production reaction from any compound on the network to _T_
			for(Compound c: network.getCompounds().values()) {
				if( c != target ) {
					Reaction r = network.addNewReaction("Rto_T_For"+c.getId(), "Rto_T_For"+c.getId(), false);
					r.addProduct(target);
					r.addSubstrate(c);
				}
			}

			// adds for each reaction a special "reaction compound" as a substrate
			for(Reaction r: reactions) {
				Compound cForR = network.addCompound(r.getId(), r.getId(), "Compound representing "+r.getId());
				r.addSubstrate(cForR);
				r.setReversible(false);
			}
		}
		
		// remove catalyst compounds (present as substrate and product of the same reaction),
		// orphans (compounds that are used by no reaction) and "empty" reaction (reaction with empty substrate or product list)
		network.removeCompoundsFromReactionsIfPresentAsSubstrateAndProduct();
		network.cleanRepeatedReactions();
		network.cleanEmptyReactions();
		network.cleanOrphanCompounds();
		
		// All compounds defined as precursors have to be transformed in topological precursors
		network.extendNonTopologicalPrecursors();

		int nRev=0;
		for(Reaction rxn : network.getReactions().values()) 
		{
			if(rxn.isReversible())
				nRev++;
		}
		System.out.println("\nNumber of reversible reactions : "+(nRev/2)+"\n");
	}

	private void readInputFile(String inputFile)	
	{
		InputParametersParser.parse(inputFile);
		
		// adding the input compounds for the forward propagation process
		for(String strId : InputParametersParser.inputForFP ) {
        	String id = StringUtils.sbmlEncode(strId);
        	Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null)
        		System.out.println("Input Compound won't find. Id = " + id);
        	else
        	{
        		InputParameters.getInputCompounds().add(c);
        	}
            System.out.println("Input compound: " + strId + " - SBML Format: " + id);         
		}

		// adding the bootstrap compounds for the forward propagation process
		for(String strId : InputParametersParser.bootstrapForFP ) {
        	String id = StringUtils.sbmlEncode(strId);
        	Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null)
        		System.out.println("Bootstrap Compound won't find. Id = " + id);
        	else
        	{
        		InputParameters.getBootstrapCompounds().add(c);
        	}
            System.out.println("Bootstrap compound: " + strId + " - SBML Format: " + id);         
		}

		// adding the user defined precursors 
		for(String strId : InputParametersParser.userDefinedPrecursor ) {
        	String id = StringUtils.sbmlEncode(strId);
        	Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null)
        		System.out.println("UD Precursor won't find. Id = " + id);
        	else
        	{
        		c.setUserDefinedPrecursor(true);
        	}
            System.out.println("UD Precursor: " + strId + " - SBML Format: " + id);         
		}		
		
		// adding the forbidden precursors
		for(String strId : InputParametersParser.forbiddenPrecursors ) {
        	String id = StringUtils.sbmlEncode(strId);
        	Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null)
        		System.out.println("Forbidden Precursor won't find. Id = " + id);
        	else
        	{
        		c.setAllowed(false);
        	}
            System.out.println("Forbidden Precursor: " + strId + " - SBML Format: " + id);         
		}		

		// adding the target precursors
		for(String strId : InputParametersParser.target ) {
        	String id = StringUtils.sbmlEncode(strId);
        	Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null)
        		System.out.println("Target won't find. Id = " + id);
        	else
        	{
        		InputParameters.getTargetCompounds().add(c);
        	}
            System.out.println("Target: " + strId + " - SBML Format: " + id);         
		}		
	}
	
	// Removes from the network all the compounds in the list of "not allowed precursors"
	public void removeForbiddenPrecursors()
	{
		List<Compound> cpdsToRemove = new ArrayList<Compound>();
		for(Compound c : network.getCompounds().values() )
		{
			if( c.isPrecursor() && !c.isAllowed() )
				cpdsToRemove.add(c);
		}
		network.removeCompounds(cpdsToRemove);
	}

	public void setNetwork(MetabolicNetwork network) {
		this.network = network;
	}
	
	/**
	 * From a set of ids, returns the list of corresponding compounds in the network
	 * @param ids
	 * @return
	 */
	public List<Compound> strToCompound(String ids[], boolean sorting) {
		List<Compound> cpds = new ArrayList<Compound>();
		
		for(String id : ids) {
			Compound c = this.getNetwork().getCompounds().get( id );
        	if( c == null)
        		System.out.println("Compound won't find. Id = " + id);
        	else
        	{
        		cpds.add(c);
        	}
		}
		
		if(sorting) {
			Collections.sort(cpds, null);
		}
		
		return cpds;
	}
	
}
