/* 
PITUFO - A software tool to find all minimal precursor sets for a given set of targets in metabolic networks.

Copyright (C) 2011 Ludovic Cottret (l.cottret@gmail.com <mailto:l.cottret@gmail.com>), Paulo Vieira Milreu  (paulovieira@milreu.com.br <mailto:paulovieira@milreu.com.br>)      
This file is part of PITUFO.

PITUFO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PITUFO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PITUFO.  If not, see <http://www.gnu.org/licenses/>.
*/
package application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metabolicNetwork.Compound;
import metabolicNetwork.Reaction;


/**
 * @author ludo
 * This class represents a solution of the findPrecursors method. 
 * It contains the set of precursors and the compounds used as bootstrap in the auto fed cycles.
 * 
 *
 */
public class PrecursorSet implements Comparable<PrecursorSet> {
	
	List<Compound> precursors;
	List<Compound> bootstraps;
	
	HashMap<String,Reaction> reactions;
	
	boolean flag;
	boolean emptyCompounds = false;
	
	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	/**
	 * Constructors
	 *
	 */
	public PrecursorSet() {
		
		precursors = new ArrayList<Compound>();
		bootstraps = new ArrayList<Compound>();
		
	}
	
	public PrecursorSet(PrecursorSet sol) {
		
		precursors = new ArrayList<Compound>(sol.getPrecursors());
		bootstraps = new ArrayList<Compound>(sol.getBootstraps());
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if( !(obj instanceof PrecursorSet) ) {
			return false;
		}
		PrecursorSet other = (PrecursorSet)obj;
		
		// check if the two objects have the same precursor set
		if( precursors.size() != other.precursors.size()) {
			return false;
		}
		List<Compound> myList = new ArrayList<Compound>(precursors);
		for(Compound c: other.precursors) {
			if( myList.contains(c) ) {
				myList.remove(c);
			}
		}
		if( myList.size() != 0 ) // if they are equal, I removed all element of the other list
			return false;
		
		// check if the two objects have the same bootstrap set
		if( bootstraps.size() != other.bootstraps.size()) {
			return false;
		}
		myList = new ArrayList<Compound>(bootstraps);
		for(Compound c: other.bootstraps) {
			if( myList.contains(c) ) {
				myList.remove(c);
			}
		}
		if( myList.size() != 0 ) // if they are equal, I removed all element of the other list
			return false;
		
		return true;
	}
	
	public boolean isMinimal(PrecursorSet sol, Boolean lookBootstraps) {
		
		if(lookBootstraps == true) {
			// We minimize the union between the set of precursors and the set of bootstraps
			
			List<Compound> compoundsInA = new ArrayList<Compound>();
			compoundsInA.addAll(this.getPrecursors());
			compoundsInA.addAll(this.getBootstraps());

			List<Compound> compoundsInB = new ArrayList<Compound>();
			compoundsInB.addAll(sol.getPrecursors());
			compoundsInB.addAll(sol.getBootstraps());
			
			return compoundsInB.containsAll(compoundsInA);
			
		}
		else {
			
			if(this.getPrecursors().equals(sol.getPrecursors())) {
				// The two solutions have the same number of precursors
				// We look for the bootstrap compounds

				if(this.getBootstraps().size() == 0)
					return false;

				return sol.getBootstraps().containsAll(this.getBootstraps());
				
			}
			else {
				
				return sol.getPrecursors().containsAll(this.getPrecursors());
				
			}
		}
		
	}
	
	public Boolean isEmpty() {
		
		return(this.getPrecursors().size() == 0 && this.getBootstraps().size() == 0);
		
	}
	
	
	public void addPrecursor(Compound p) {
		this.getPrecursors().add(p);
	}
	
	public void addBootstrap(Compound b) {
		this.getBootstraps().add(b);
	}
	
	public void add(PrecursorSet sol) {
		this.getPrecursors().addAll(sol.getPrecursors());
		this.getBootstraps().addAll(sol.getBootstraps());
	}

	public List<Compound> getBootstraps() {
		return bootstraps;
	}

	public void setBootstraps(List<Compound> bootstraps) {
		this.bootstraps = bootstraps;
	}

	public List<Compound> getPrecursors() {
		return precursors;
	}

	public void setPrecursors(List<Compound> precursors) {
		this.precursors = precursors;
	}
	
	public HashMap<String, Reaction> getReactions() {
		return reactions;
	}

	public void setReactions(HashMap<String, Reaction> reactions) {
		this.reactions = reactions;
	}

	public String toString() {
		return "Precursors: "+precursors;
	}

	@Override
	public int compareTo(PrecursorSet ps) {
		return toString().compareTo(ps.toString());
	}

	public boolean isEmptyCompounds() {
		return emptyCompounds;
	}

	public void setEmptyCompounds(boolean emptyCompounds) {
		this.emptyCompounds = emptyCompounds;
	}
	
}
