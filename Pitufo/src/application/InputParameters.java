package application;

import java.util.ArrayList;
import java.util.List;

import metabolicNetwork.Compound;

public class InputParameters {
	
	private static List<Compound> inputCompounds = new ArrayList<Compound>();
	private static List<Compound> bootstrapCompounds = new ArrayList<Compound>();
	private static List<Compound> userDefinedPrecursors = new ArrayList<Compound>();
	private static List<Compound> targetCompounds = new ArrayList<Compound>();

	public static boolean decodeSbml = true;
	public static boolean printForward = false;
	public static boolean printNetwork = false;
	public static boolean oneByOne = true;
	public static boolean keepForwardResultInTheTree = false;
	public static boolean minimalityCheck = true;
	public static boolean mergeOnlyWithMinimalReactions = true;
	public static boolean forwardPropagation = false;
	public static boolean reductionThroughMaximalTarget = false;
	public static boolean precursorIfProducedOnlyByReversible = true;
	public static boolean checkSolutions = false;
	public static boolean mergeKeepingSideCompounds = true;
	public static int     maxTimePerTarget = 0;//1 * 60 * 1000;
	public static boolean emptyCyclesEnumeration = false;
	public static boolean emptyCyclesCut = false;
	public static boolean randomChoices = true;
	public static int     stopNoNew = 0;
	
	public static List<Compound> getInputCompounds() {
		return inputCompounds;
	}
	public static void setInputCompounds(List<Compound> inputCompounds) {
		InputParameters.inputCompounds = inputCompounds;
	}
	public static List<Compound> getUserDefinedPrecursors() {
		return userDefinedPrecursors;
	}
	public static void setUserDefinedPrecursors(List<Compound> userDefinedPrecursors) {
		InputParameters.userDefinedPrecursors = userDefinedPrecursors;
	}
	public static List<Compound> getBootstrapCompounds() {
		return bootstrapCompounds;
	}
	public static void setBootstrapCompounds(List<Compound> bootstrapCompounds) {
		InputParameters.bootstrapCompounds = bootstrapCompounds;
	}
	public static List<Compound> getTargetCompounds() {
		return targetCompounds;
	}
	public static void setTargetCompounds(List<Compound> targetCompounds) {
		InputParameters.targetCompounds = targetCompounds;
	}

}
